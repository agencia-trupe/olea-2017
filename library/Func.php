<?php
/**
 * Funções genéricas
 */
class Func
{
	public function img($img)
	{
		return "<img src=\"".ROOT_PATH.$img."\" />";
	}
	
	public function removeDecorators(&$elm)
    {
        $elm->removeDecorator('label')
			->removeDecorator('htmlTag')
            ->removeDecorator('description')
            ->removeDecorator('errors');
    }
	
	public function clean($val)
	{
		return str_replace(array('.',',',' ','-','_','(',')','[',']'),'',trim($val));
	}
	
	public function br($j)
	{
		$br = "";
		for($i=0;$i<=$j;$i++){
			$br.= "<br/>";
		}
		return $br;
	}
    
    public static function _toUtf8(&$item, $key) {
        $item = iconv("iso-8859-1","utf-8",$item);
    }
    
    public static function _toIso(&$item, $key) {
        $item = iconv("utf-8","iso-8859-1",$item);
    }
    
    public function arrayToObject($array)
    {
        $object = new stdClass();
        if (is_array($array)){
            foreach ($array as $name=>$value){
                $name = trim($name);
                if (!empty($name)){
                    $object->$name = $value;
                }
            }
        } else {
            //die("param is not an array.");
        }
        return $object;
    }
    
    public function objectToArray($object)
    {
        $array = array();
        if (is_object($object)){
            $array = get_object_vars($object);
        }
        return $array;
    }
    
    public static function _encodeUtf8(&$item,$key)
	{
		$item = utf8_encode($item);
	}
    
    public static function _decodeUtf8(&$item,$key)
	{
		$item = utf8_decode($item);
	}
    
    public static function _arrayToObject(&$item,$key)
	{
		$item = self::arrayToObject($item);
	}
    
    public function drop($name=null,$belong,$options,$value='',$class='f1')
	{
		array_walk($options,'Func::_encodeUtf8');
		$drop = new Zend_Form_Element_Select($belong,$name ? array('belongsTo' => $name) : array());
		$drop->setAttrib('class',$class)
			->addMultiOptions($options)
			->setValue($value);
		Func::removeDecorators($drop);
		return $drop;
	}
    
    public function chk_cb($val1,$val2)
    {
        return $val1 == $val2 ? 'checked="checked"' : '';
    }
    
    public function chk_drop($val1,$val2)
    {
        return $val1 == $val2 ? 'selected="selected"' : '';
    }
}

class Img
{
    public function bg($path,$preload=false)
    {
        $path = URL."/img/".str_replace("/","|",$path);
        $style = "style=\"background-image:url('$path');\"";
        
        if($preload){
            $img = 'img_'.rand(0,999999);
            $script = Js::script("var $img = new Image(); $img.src = '$path';");
            return $style.'>'.substr($script,0,strlen($script)-2);
        }
        
        return $style;
    }
    
    public function src($path,$attrs="")
    {
        return "<img src='".URL."/img/".str_replace("/","|",$path)."' ".$attrs." />";
    }
}

class Image
{
    public function bg($path)
    {
        return "style=\"background-image:url('".CSS_PATH."/img/".$path."');\"";
    }
    
    public function src($path,$attrs="",$default_path=true)
    {
        $path = ($default_path ? IMG_PATH."/" : "").$path;
        return "<img src='".$path."' ".$attrs." />";
    }
}

class Youtube
{
	public function checkStr($str)
	{
		$regex = "#youtu(be.com|.b)(/v/|/watch\\?v=|e/|/watch(.+)v=)(.{11})#";
		preg_match_all($regex , $str, $m);
		return $m;
	}
	
	public function hasVideo($str)
	{
		$s = self::checkStr($str);
		return count($s[0]) ? $s : false;
	}
	
	public function getText($str)
	{
		if(!$s = self::hasVideo($str)) return $str;
		$p = '/(.*)(http.*)/';
		preg_match($p,$str,$m);
		return $m[1];
	}
	
	public function getUrl($str)
	{
		if(!$s = self::hasVideo($str)) return null;
		return $s[0][0];
	}
	
	public function getCode($str)
	{
		if(!$s = self::hasVideo($str)) return null;
		return $s[4][0];
	}

	public function embed($code,$width=640,$height=480)
	{
		$url = 'http://www.youtube.com/embed/'.$code.'?autohide=1';
		return '<iframe '.
				'width="'.$width.'" '.
				'height="'.$height.'" '.
				'src="'.$url.'" '.
				'frameborder="0" '.
				'allowfullscreen></iframe>';
	}
	
	public function parseStr($str)
	{
		$o = new stdClass();
		$o->text = self::getText($str);
		$o->url  = self::getUrl($str);
		$o->code = self::getCode($str);
		return $o;
	}
}

class Vimeo
{
	public function checkStr($str)
	{
		$regex = "#vimeo(.com|.b)(/)(\d{8,})#";
		preg_match_all($regex , $str, $m);
		return $m;
	}
	
	public function hasVideo($str)
	{
		$s = self::checkStr($str);
		return count($s[0]) ? $s : false;
	}
	
	public function getText($str)
	{
		if(!$s = self::hasVideo($str)) return $str;
		$p = '/(.*)(http.*)/';
		preg_match($p,$str,$m);
		return $m[1];
	}
	
	public function getUrl($str)
	{
		if(!$s = self::hasVideo($str)) return null;
		return $s[0][0];
	}
	
	public function getCode($str)
	{
		if(!$s = self::hasVideo($str)) return null;
		return $s[3][0];
	}

	public function embed($code,$width=640,$height=480)
	{
		$url = 'http://player.vimeo.com/video/'.$code.'';
		return '<iframe '.
				'width="'.$width.'" '.
				'height="'.$height.'" '.
				'src="'.$url.'" '.
				'frameborder="0" '.
				'allowfullscreen></iframe>';
	}
	
	public function parseStr($str)
	{
		$o = new stdClass();
		$o->text = self::getText($str);
		$o->url  = self::getUrl($str);
		$o->code = self::getCode($str);
		return $o;
	}
}

function _checkVideoStr($str){
	if(strstr($str,'vimeo.com')) return Vimeo::parseStr($str);
	if(strstr($str,'youtube.com')) return Youtube::parseStr($str);
	return false;
}

function _iframeVideo($url,$width=640,$height=480){
	if(strstr($url,'vimeo.com')) return Vimeo::embed(Vimeo::getCode($url),$width,$height);
	if(strstr($url,'youtube.com')) return Youtube::embed(Youtube::getCode($url),$width,$height);
	return false;
}

function _d($var,$exit=true){
    return Is_Var::dump($var,$exit);
}

function brMiddle($str)
{
	$parts = explode(' ', $str);
	$cnt = count($parts);
	$mid = floor($cnt/2);
	$str2 = '';
	for($i=0;$i<$cnt;$i++)
		$str2.= ($i==$mid ? '<br>' : ' ').$parts[$i];
	return $str2;
}

function cleanHtml($text){
	// $tags = '<b><strong><p><div><a><i><u><ul><ol><li><img><br><h1><h2><h3><h4><h5><h6><table><thead><tbody><th><tr><td><hr>';
	$tags = 'b,strong,p,div,a,i,u,ul,ol,li,img,br,h1,h2,h3,h4,h5,h6,'.
			'table,thead,tbody,th,tr,td,hr,strike';
	$tags = '<'.implode('><',explode(',',$tags)).'>';
	$text = stripslashes(($text));
	$text = (strip_tags($text,$tags));
    
    // replace contents simple text
    $replaces = array(
        '“' => '"',
        '”' => '"',
        '•' => '-',
    );
    foreach($replaces as $p => $r) $text = str_replace($p,$r,$text);
    
    // replace contents
    $replaces = array(
    	//'\<a(.*)\<\/a\>' => '<a target="_blank" rel="nofollow" \\1</a>',
    	// '<([a-zA-Z0-9]*)(.*)(/?)(.*)>' => '<\\1 \\3>',
    	// 'style\=\"(.*)\"' => ' ',
	);
    foreach($replaces as $p => $r) $text = ereg_replace($p,$r,$text);

    // import html
    Lib::import('phpQuery');
    $html = phpQuery::newDocumentHTML($text);

    // add attributes
    $adds = array(
    	'a' => array('target'=>'_blank','rel'=>'nofollow')
	);
    foreach($adds as $p => $r) foreach($r as $k => $v) $html->find($p)->attr($k,$v);

    // clean styles
    $allow_styles = array(
    	'text-align: center',
    	'text-align: left',
    	'text-align: right',
    	'text-align: justify',
    	'width: 100%'
    );
    
    foreach (explode('><',substr($tags,1,-1)) as $tag) {
    	$items = $html->find($tag);
    	
    	foreach ($items as $item) {
    		$style = array();

    		foreach ($allow_styles as $as) {
    			if(strstr(pq($item)->attr('style'), $as)) $style[] = $as;
    		}

    		(count($style)) ? 
    			pq($item)->attr('style',implode(';',$style)) : 
    			pq($item)->removeAttr('style');

    		pq($item)->removeAttr('face');
    	}
    }
	
	return $html->html();
	// return $text;
}