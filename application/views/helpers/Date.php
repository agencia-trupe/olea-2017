<?php
/**
 * Formatação de Datas
 * Auxiliar da Camada de Visualização
 * @author Wanderson Henrique Camargo Rosa
 * @see APPLICATION_PATH/views/helpers/Date.php
 */
class Zend_View_Helper_Date extends Zend_View_Helper_Abstract
{
    /**
     * Manipulador de Datas
     * @var Zend_Date
     */
    protected static $_date = null;
    protected static $_locale = null;
 
    /**
     * Método Principal
     * @param string $value Valor para Formatação
     * @param string $format Formato de Saída
     * @return string Valor Formatado
     */
    public function date($value, $format = Zend_Date::DATETIME_MEDIUM)
    {
        // if($this->_locale=='en') $value = Is_Date::am2en($value);
        $date = $this->getDate();
        return $date->set(strtotime($value))->get($format,$this->_locale);
    }
 
    /**
     * Acesso ao Manipulador de Datas
     * @return Zend_Date
     */
    public function getDate()
    {
        if (self::$_date == null) {
            // $locale = $this->_locale = new Zend_Locale(Zend_Registry::get('Zend_Locale'));
            // self::$_date = new Zend_Date(null,Zend_Locale_Format::getDateFormat($locale),$locale);
            self::$_date = new Zend_Date();
        }
        return self::$_date;
    }
}