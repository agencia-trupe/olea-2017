<?php
/**
 * 
 */
class Zend_View_Helper_Produtos extends Zend_View_Helper_Abstract
{
    public $info = null;
    
    public function setInfo($info)
    {
        $this->info = $info;
    }
    
    public function hasInfo()
    {
        return $this->info === null ? false : true;
    }
    
    public function getInfo($key)
    {
        return $this->hasInfo() ? $this->info[$key] : '0';
    }
}