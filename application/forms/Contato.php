<?php

class Application_Form_Contato extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')
             ->setAction(URL.'/contato/enviar/')
             ->setAttrib('id','frm-fale-conosco')
             ->setAttrib('name','frm-fale-conosco');
        
        // elementos
        $this->addElement('text','nome',array('label'=>'nome','class'=>'txt2'));
        $this->addElement('text','email',array('label'=>'e-mail','class'=>'txt2'));
        $this->addElement('text','telefone',array('label'=>'telefone','class'=>'txt2 mask-tel'));
        $this->addElement('text','assunto',array('label'=>'assunto','class'=>'txt2'));
        $this->addElement('textarea','mensagem',array('label'=>'mensagem','class'=>'txt2'));
        
        // filtros / validações
        $this->getElement('nome')->setRequired()
             ->addFilter('StripTags')
             ->addFilter('StringTrim');
        $this->getElement('mensagem')->setRequired()
             ->addFilter('StripTags')
             ->addFilter('StringTrim');
        $this->getElement('email')->setRequired()
             ->addValidator('EmailAddress')
             ->addFilter('StripTags');
        
        // remove decoradores
        $this->removeDecs();
    }
}

