<?php

class Admin_BlogsPostsController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        $this->view->titulo = "BLOG";
        $this->view->section = $this->section = "blogs-posts";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".FILE_PATH."/".$this->section;
        
        // models
        $this->blogs = new Application_Model_Db_Blogs();
        $this->noticias = new Application_Model_Db_BlogsPosts();
        $this->login = $this->view->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();

        if(!$this->_hasParam('alias')){
            $this->_forward('not-found','error','default',array('url'=>URL.'/admin'));
            return false;
        }

        $this->view->blog = $this->blog = Is_Array::utf8DbRow($this->blogs->fetchRow('alias="'.$this->_getParam('alias').'"'));

        if(!$this->blog){
            $this->_forward('not-found','error','default',array('url'=>URL.'/admin'));
            return false;
        }
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/".$this->blog->alias;
        $this->view->titulo = "<a href='".$this->_url."'>BLOG ".$this->blog->titulo."</a>";

        Application_Model_Login::checkAuth($this);

        if($this->login->user->role == 3 && $this->blog->user_id != $this->login->user->id){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin'));
        }
    }

    public function indexAction()
    {
        /* paginação */
        $records_per_page   = 10;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        $where = 'blog_id='.$this->blog->id;

        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            $where.= ' and '.$post['search-by']." like '%".utf8_decode($post['search-txt'])."%'";
            $rows = $this->noticias->fetchAll($where,'data_edit desc',$limit,$offset);
            
            $total = $this->view->total = $this->noticias->count($where);
        } else {
            $rows = $this->noticias->fetchAll($where,'data_edit desc',$limit,$offset);
            $total = $this->view->total = $this->noticias->count($where);
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        
        $this->view->rows = Is_Array::utf8DbResult($rows);
        $form = new Admin_Form_BlogsPosts();
        $this->view->categorias = $form->getElement('categoria_id')->options;
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR POST":" &rarr; NOVO POST");
        $form = new Admin_Form_BlogsPosts();
        // _d(array(URL.$this->_url.'/save/',$form->getAction()));
        $form->setAction($this->_url.'/save/');
        
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            if((bool)$data['data']) $data['data'] = Is_Date::am2br($data['data']);
            $this->view->id = $this->noticia_id = $data['id'];
            $form->addElement('hidden','id');
            //$form->removeElement('allow_photos');
            $this->view->fotos = $this->fotosAction();
            $this->view->allow_photos = $data['allow_photos'];

            if(!(bool)$this->blog->has_categoria) $form->removeElement('categoria_id');
        } else {
            $form->removeElement('body_pt');
            $form->removeElement('body_en');
            //$form->removeElement('status_id');
            $data = array('status_id'=>'1','allow_photos'=>'1');
        }

        if($this->login->user->role == 3){
            $form->removeElement('status_id');
        }
        
        $form->populate($data);
        $this->view->form = $form;
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url.'/new'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->noticias->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $data = array_map('utf8_decode',$this->_request->getParams());
            $data['alias'] = Is_Str::toUrl($this->_getParam('titulo_pt'));
            $data['body_pt']  = isset($data['body_pt']) ? strip_tags($data['body_pt'],'<b><a><i><u><br><ul><ol><li><img><p><div>') : null;
            $data['body_en']  = isset($data['body_en']) ? strip_tags($data['body_en'],'<b><a><i><u><br><ul><ol><li><img><p><div>') : null;
            $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
            $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
            $data['data_edit'] = date("Y-m-d H:i:s");
            $data['allow_photos'] = 1;
            $data['data'] = ((bool)$data['data']) ? Is_Date::br2am($data['data']) : null;
            $data['blog_id'] = $this->blog->id;

            if(!$row && $this->login->user->role == 3){
                $data['status_id'] = $this->blog->moderacao_posts==1 ? 0 : 1;
            }

            // remove dados desnecessários
            if(isset($data['submit'])){ unset($data['submit']); }
            if(isset($data['module'])){ unset($data['module']); }
            if(isset($data['controller'])){ unset($data['controller']); }
            if(isset($data['action'])){ unset($data['action']); }
            
            ($row) ? $this->noticias->update($data,'id='.$id) : $id = $this->noticias->insert($data);
            
            $this->messenger->addMessage('Registro atualizado.');
            $data['id'] = $id;
            // $this->_redirect($this->_url.'/edit/'.$id.'/');
            $this->_redirect(URL.'/admin/'.$this->section.'/'.$this->blog->alias.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>Is_Array::utf8All($data)));
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ? 'Já existe uma notícia com este título. Por favor, escolha um novo.' : $e->getMessage();
            $this->messenger->addMessage($error,'error');
            $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->noticias->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('not-found','error','default',array('url'=>$this->_url));return false; }
        $this->_forward('new',null,null,array('data'=>Is_Array::utf8All($row->toArray())));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        
        try {
            $this->noticias->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('blogs_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->order('f2.id asc');
        
        if(isset($this->noticia_id)){
            $select->where('f2.blog_post_id = ?',$this->noticia_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            $fotos->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        $max_size = '5120'; // '2048'
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/'));
            return;
        }
        
        $file = $_FILES['file'];
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
               ->addValidator('Size', false, array('max' => $max_size.'kb'))
               ->addValidator('Count', false, 1)
               ->addFilter('Rename',$this->img_path.'/'.$rename);
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.Is_File::formatBytes($max_size).'.');
        }
        
        try {
            $upload->receive();
            
            $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
            $thumb->resize('1000','1000');
            $thumb->save($this->img_path.'/'.$rename);
            
            $fotos = new Application_Model_Db_Fotos();
            $noticias_fotos = new Application_Model_Db_BlogsFotos();
            $noticia_id = $this->_getParam('id');
            
            $data_fotos = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$foto_id = $fotos->insert($data_fotos)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            $noticias_fotos->insert(array("foto_id"=>$foto_id,"blog_post_id"=>$noticia_id));
            
            return array("name"=>$rename,"id"=>$foto_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }

}