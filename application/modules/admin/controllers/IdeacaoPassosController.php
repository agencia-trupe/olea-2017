<?php

class Admin_IdeacaoPassosController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "IDEAÇÃO &rarr; PASSO A PASSO";
        $this->view->section = $this->section = "ideacao-passos";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/"."/".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/"."/".FILE_PATH."/".$this->section;
        
        // models
        $this->noticias = new Application_Model_Db_IdeacaoPassos();
        $this->paginas = new Application_Model_Db_PaginaHome();
        // $this->tags = new Application_Model_Db_Tags();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();
    }

    public function indexAction()
    {
        if($this->_hasParam('pagina_id')) $this->view->pagina_id = $this->_getParam('pagina_id');

        /* paginação */
        $records_per_page   = 10;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            $where = $post['search-by']." like '%".utf8_decode($post['search-txt'])."%'";

            if($this->_hasParam('pagina_id')) $where.= ' and pagina_id='.$this->view->pagina_id;

            $rows = $this->noticias->fetchAll($where,'data_edit desc',$limit,$offset);
            
            $total = $this->view->total = $this->noticias->count($where);
        } else {
            $where = null;
            if($this->_hasParam('pagina_id')) $where = 'pagina_id='.$this->view->pagina_id;
            $rows = $this->noticias->fetchAll($where,'data_edit desc',$limit,$offset);
            $total = $this->view->total = $this->noticias->count();
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        
        $this->view->rows = Is_Array::utf8DbResult($rows);
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; NOVO");
        $form = new Admin_Form_IdeacaoPassos();
        
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            $this->view->id = $this->noticia_id = $data['id'];
            $form->addElement('hidden','id');
            //$form->removeElement('allow_photos');
            $this->view->fotos = $this->fotosAction();
            $this->view->arquivos = $this->arquivosAction();
            $this->view->allow_photos = $data['allow_photos'];

            // tags
            // $this->view->tags = $this->tagsAction();
            // $this->view->tags_list = $this->noticias->getTags($data['id']);
            $pagina_id = $data['pagina_id'];
        } else {
            if(!$this->_hasParam('pagina_id')) return $this->_forward('denied','error','default',array('url'=>$this->_url));
            $form->removeElement('body_pt');
            $form->removeElement('body_en');
            //$form->removeElement('status_id');
            $data = array('status_id'=>'1','allow_photos'=>'1','allow_files'=>'1','pagina_id'=>$this->_getParam('pagina_id'));
            $pagina_id = $this->_getParam('pagina_id');
        }

        $pagina = Is_Array::utf8DbRow($this->paginas->fetchRow('id='.$pagina_id));
        
        $form->populate($data);
        $this->view->form = $form;
        $this->view->pagina = $pagina;
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url.'new/'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->noticias->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $data = array_map('utf8_decode',$this->_request->getParams());
            $data['alias'] = Is_Str::toUrl($this->_getParam('titulo_pt'));
            $data['olho_pt']  = isset($data['olho_pt']) ? addslashes($data['olho_pt']) : null;
            $data['olho_en']  = isset($data['olho_en']) ? addslashes($data['olho_en']) : null;
            $data['body_pt']  = isset($data['body_pt']) ? strip_tags($data['body_pt'],'<b><a><i><u><br><ul><ol><li><img><p><div><h3>') : null;
            $data['body_en']  = isset($data['body_en']) ? strip_tags($data['body_en'],'<b><a><i><u><br><ul><ol><li><img><p><div><h3>') : null;
            $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
            $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
            $data['data_edit'] = date("Y-m-d H:i:s");
            $data['allow_photos'] = 1;
            $data['allow_files'] = 1;
            
            // remove dados desnecessários
            if(isset($data['submit'])){ unset($data['submit']); }
            if(isset($data['module'])){ unset($data['module']); }
            if(isset($data['controller'])){ unset($data['controller']); }
            if(isset($data['action'])){ unset($data['action']); }
            
            ($row) ? $this->noticias->update($data,'id='.$id) : $id = $this->noticias->insert($data);
            
            $this->messenger->addMessage('Registro atualizado.');
            $data['id'] = $id;
            $this->_redirect('admin/'.$this->section.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>Is_Array::utf8All($data)));
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ? 'Já existe um texto com este título. Por favor, escolha um novo.' : $e->getMessage();
            $this->messenger->addMessage($error,'error');
            $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->noticias->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('not-found','error','default',array('url'=>$this->_url));return false; }
        $this->_forward('new',null,null,array('data'=>Is_Array::utf8All($row->toArray())));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        
        try {
            $this->noticias->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('etapas_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->order('f2.id asc');
        
        if(isset($this->noticia_id)){
            $select->where('f2.etapa_id = ?',$this->noticia_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            $fotos->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        $max_size = '5120'; // '2048'
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/'.$this->section.'/'));
            return;
        }

        $file = $_FILES['file'];
        $isFile = $this->_hasParam('arquivo');
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();

        if($isFile){
            $upload->addFilter('Rename',$this->file_path.'/'.$rename)
                   ->setDestination($this->file_path);
        } else {
            $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
                   ->addValidator('Size', false, array('max' => $max_size.'kb'))
                   ->addValidator('Count', false, 1)
                   ->addFilter('Rename',$this->img_path.'/'.$rename)
                   ->setDestination($this->img_path);
        }
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.Is_File::formatBytes($max_size).'.');
        }
        
        try {
            $upload->receive();
            
            $table  = $isFile ? new Application_Model_Db_Arquivos() : new Application_Model_Db_Fotos();
            $table2 = $isFile ? new Application_Model_Db_EtapasArquivos() : new Application_Model_Db_EtapasFotos();
            $type   = $isFile ? "arquivo" : "foto";
            $case_id = $this->_getParam('id');

            if(!$isFile){
                $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
                $thumb->resize('1000','1000');
                $thumb->save($this->img_path.'/'.$rename);
            }
            
            $data_insert = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$insert_id = $table->insert($data_insert)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            $table2->insert(array($type."_id"=>$insert_id,"etapa_id"=>$case_id));
            
            return array("name"=>$rename,"id"=>$insert_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }

    public function tagsAction()
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $rows = $select->from('tags')/*->where('categoria_id=30')*/->order('alias')->query()->fetchAll();
        $rows = array_map('Is_Array::utf8All',$rows);
        return $rows;
    }

    public function tagsAddAction()
    {
        if($this->_hasParam('etapa_id')){
            $noticias_sugestoes = new Application_Model_Db_EtapasTags();
            try{
                $tag_id = $this->_getParam('tag_id');

                if($this->_hasParam('tag_tag')){
                    $tag_tag = trim($this->_getParam('tag_tag'));
                    $tag_alias = Is_Str::toUrl(trim($tag_tag));

                    if($rowTag = $this->tags->fetchRow('alias="'.$tag_alias.'"')){
                        $tag_id = $rowTag->id;
                    } else {
                        $tag_data = array(
                            'tag' => Is_Str::toLower(utf8_decode($tag_tag)),
                            'alias' => $tag_alias
                        );
                        $tag_id = $this->tags->insert($tag_data);
                    }

                }

                $noticias_sugestoes->insert(array(
                    'etapa_id'=>$this->_getParam('etapa_id'),
                    'tag_id'=>$tag_id
                ));
                return array();
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),"Duplicate") ? "Registro duplicado." : $e->getMessage();
                return array('error'=>$msg);
            }
        } else {
            return array('error'=>'Não foi possível adicionar tag.');
        }
    }
    
    public function tagsDelAction()
    {
        if($this->_hasParam('etapa_id') && $this->_hasParam('tag_id')){
            $etapas_sugestoes = new Application_Model_Db_EtapasTags();
            $etapas_sugestoes->delete('etapa_id="'.$this->_getParam('etapa_id').'"'.
                                        'and tag_id="'.$this->_getParam('tag_id').'"');
            return array();
        } else {
            return array('error'=>'Não foi possível remover tag.');
        }
    }

    public function arquivosDelAction()
    {
        $id = $this->_getParam("file");
        $arquivos = new Application_Model_Db_Arquivos();
        $arquivo = $arquivos->fetchRow('id='.(int)$id);
                
        try {
            $arquivos->delete("id=".(int)$id);
            Is_File::del($this->file_path.'/'.$arquivo->path);
            Is_File::delDerived($this->file_path.'/'.$arquivo->path);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function arquivosRenameAction()
    {
        $id = $this->_getParam("file");
        $descricao = $this->_getParam("descricao");
        $arquivos = new Application_Model_Db_Arquivos();
        $arquivo = $arquivos->fetchRow('id='.(int)$id);
                
        try {
            $arquivos->update(array('descricao'=>utf8_decode(urldecode($descricao))),"id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function arquivosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('etapas_arquivos as tf')
            ->join('arquivos as f','f.id=tf.arquivo_id')
            ->order('f.descricao asc');
        
        if(isset($this->noticia_id)){
            $select->where('etapa_id = ?',$this->noticia_id);
        }
        
        $arquivos = $select->query()->fetchAll();
        
        array_walk($arquivos,'Func::_arrayToObject');
        
        return $arquivos;
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }

}