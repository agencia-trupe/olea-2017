<?php

class Admin_BlogsCommentsController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        Application_Model_Login::checkAuth($this);

        $this->view->titulo = "COMENTÁRIOS";
        $this->view->section = $this->section = "blogs-comments";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/..".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/..".FILE_PATH."/".$this->section;
        
        // models
        $this->blogs = new Application_Model_Db_Blogs();
        $this->noticias = new Application_Model_Db_BlogsComments();
        $this->blogs_posts = new Application_Model_Db_BlogsPosts();
        $this->usuarios = new Application_Model_Db_Clientes();
        $this->login = $this->view->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();

        if(!$this->_hasParam('alias')){
            $this->_forward('not-found','error','default',array('url'=>URL.'/admin'));
            return false;
        }

        $this->view->blog = $this->blog = Is_Array::utf8DbRow($this->blogs->fetchRow('alias="'.$this->_getParam('alias').'"'));

        if(!$this->blog){
            $this->_forward('not-found','error','default',array('url'=>URL.'/admin'));
            return false;
        }

        $this->blog->posts = Is_Array::utf8DbResult($this->blogs_posts->fetchAll('blog_id='.$this->blog->id));
        $this->users = Is_Array::utf8DbResult($this->usuarios->fetchAll());//'role not in (0)'));

        // keyValues p/ montar combo
        $blog_posts = array();
        foreach($this->blog->posts as $post) $blog_posts[$post->id] = $post->titulo;
        $this->view->blog_posts = $blog_posts;

        $usuarios = array();
        foreach($this->users as $user) $usuarios[$user->id] = $user->nome;
        $this->view->usuarios = $usuarios;

        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/".$this->blog->alias;
        $this->view->titulo = "<a href='".$this->_url."'>COMENTÁRIOS EM BLOG ".$this->blog->titulo."</a>";

        Application_Model_Login::checkAuth($this);

        if($this->login->user->role == 3 && $this->blog->user_id != $this->login->user->id){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin'));
        }
    }

    public function indexAction()
    {
        /* paginação */
        $records_per_page   = 10;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        
        $post_ids = array();
        foreach($this->blog->posts as $post) $post_ids[] = $post->id;
        $where = 'blog_post_id '.(count($post_ids)?'in ('.implode(',',$post_ids).')':'= 0');//blog_id='.$this->blog->id;
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            $where.= ' and '.$post['search-by'].(in_array($post['search-by'],array('blog_post_id','user_cad'))?' = "'.$post['search-txt'].'"':" like '%".utf8_decode($post['search-txt'])."%'");
            $rows = $this->noticias->fetchAll($where,'data_cad desc',$limit,$offset);
            
            $total = $this->view->total = $this->noticias->count($where);
        } else {
            $rows = $this->noticias->fetchAll($where,'data_cad desc',$limit,$offset);
            $total = $this->view->total = $this->noticias->count($where);
        }
        
        if(count($rows)){
            $rows = Is_Array::utf8DbResult($rows);

            foreach($rows as &$row){
                // $row->post = Is_Array::utf8DbRow($this->blogs_posts->fetchRow('id='.$row->blog_post_id));
                // $row->user = Is_Array::utf8DbRow($this->usuarios->fetchRow('id='.$row->user_cad));
                $row->post = $this->_getPost($row->blog_post_id);
                $row->user = $this->_getUser($row->user_cad);
            }
        }

        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        
        $this->view->rows = $rows;
    }
    
    public function newAction()
    {
        Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR COMENTÁRIO":" &rarr; NOVO COMENTÁRIO");
        $form = new Admin_Form_BlogsComments();
        $form->setAction($this->_url.'/save/');
        
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            // if((bool)$data['data']) $data['data'] = Is_Date::am2br($data['data']);
            $this->view->id = $this->noticia_id = $data['id'];
            $form->addElement('hidden','id');
            //$form->removeElement('allow_photos');
            $this->view->fotos = $this->fotosAction();
            $this->view->allow_photos = 0;//$data['allow_photos'];
        } else {
            $form->removeElement('body');
            //$form->removeElement('status_id');
            $data = array('status'=>'1');//,'allow_photos'=>'1');
        }
        
        $form->populate($data);
        $this->view->form = $form;
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url.'/new'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->noticias->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $data = array_map('utf8_decode',$this->_request->getParams());
            // $data['alias'] = Is_Str::toUrl($this->_getParam('titulo'));
            $data['comentario']  = isset($data['comentario']) ? strip_tags($data['comentario'],'<b><a><i><u><br><ul><ol><li><img><p><div>') : null;
            $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
            $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
            $data['data_edit'] = date("Y-m-d H:i:s");
            // $data['blog_post_id'] = $this->blog->id;

            // remove dados desnecessários
            if(isset($data['alias'])){ unset($data['alias']); }
            if(isset($data['submit'])){ unset($data['submit']); }
            if(isset($data['module'])){ unset($data['module']); }
            if(isset($data['controller'])){ unset($data['controller']); }
            if(isset($data['action'])){ unset($data['action']); }
            
            ($row) ? $this->noticias->update($data,'id='.$id) : $id = $this->noticias->insert($data);
            
            if($this->isAjax()) return array('msg'=>'Registro atualizado.');
            $this->messenger->addMessage('Registro atualizado.');
            $data['id'] = $id;
            // $this->_redirect($this->_url.'/edit/'.$id);
            $this->_redirect(URL.'/admin/'.$this->section.'/'.$this->blog->alias.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>Is_Array::utf8All($data)));
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ? 'Já existe uma notícia com este título. Por favor, escolha um novo.' : $e->getMessage();
            if($this->isAjax()) return array('error'=>$error);

            $this->messenger->addMessage($error,'error');
            $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function saveStatusAction()
    {
        if(!$this->_request->isPost() || !$this->_hasParam("id")){
            if($this->isAjax()) return array('error'=>1,'msg'=>'Acesso negado.');
            $this->_forward('denied','error','default',array('url'=>$this->_url.'/new'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->noticias->fetchRow('id='.$id); // verifica registro
        
        if(!$row){
            if($this->isAjax()) return array('error'=>1,'msg'=>'Acesso negado.');
            $this->_forward('denied','error','default',array('url'=>$this->_url.'/new'));
            return;
        }

        try {
            $row->status = $this->_getParam("status");
            $row->save();
            
            if($this->isAjax()) return array('error'=>0,'msg'=>'Status atualizado.');
            $this->messenger->addMessage('Status atualizado.');
            $data['id'] = $id;
            // $this->_redirect($this->_url.'/edit/'.$id);
            $this->_redirect(URL.'/admin/'.$this->section.'/'.$this->blog->alias.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>Is_Array::utf8All($data)));
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ? 'Já existe uma notícia com este título. Por favor, escolha um novo.' : $e->getMessage();
            if($this->isAjax()) return array('error'=>1,'msg'=>$error);

            $this->messenger->addMessage($error,'error');
            $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->noticias->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('not-found','error','default',array('url'=>$this->_url));return false; }
        $this->_forward('new',null,null,array('data'=>Is_Array::utf8All($row->toArray())));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        
        try {
            $this->noticias->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('blogs_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->order('f2.id asc');
        
        if(isset($this->noticia_id)){
            $select->where('f2.blog_post_id = ?',$this->noticia_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            $fotos->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        $max_size = '5120'; // '2048'
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/noticias/'));
            return;
        }
        
        $file = $_FILES['file'];
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
               ->addValidator('Size', false, array('max' => $max_size.'kb'))
               ->addValidator('Count', false, 1)
               ->addFilter('Rename',$this->img_path.'/'.$rename);
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.Is_File::formatBytes($max_size).'.');
        }
        
        try {
            $upload->receive();
            
            $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
            $thumb->resize('1000','1000');
            $thumb->save($this->img_path.'/'.$rename);
            
            $fotos = new Application_Model_Db_Fotos();
            $noticias_fotos = new Application_Model_Db_BlogsFotos();
            $noticia_id = $this->_getParam('id');
            
            $data_fotos = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$foto_id = $fotos->insert($data_fotos)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            $noticias_fotos->insert(array("foto_id"=>$foto_id,"blog_post_id"=>$noticia_id));
            
            return array("name"=>$rename,"id"=>$foto_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }

    private function _getUser($id)
    {
        $row = null;

        foreach($this->users as &$user){
            if($user->id == $id){
                $row = $user;
                break;
            }
        }

        return $row;
    }

    private function _getPost($id)
    {
        $row = null;

        foreach($this->blog->posts as &$post){
            if($post->id == $id){
                $row = $post;
                break;
            }
        }

        return $row;
    }

}