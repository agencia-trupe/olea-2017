<?php

class Admin_EsqueciMinhaSenhaController extends Zend_Controller_Action
{
    public function init()
    {
        $this->view->titulo = "Esqueci minha senha";
        $this->view->section = $this->section = "esqueci-minha-senha";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/";
    }

    /**
     * Primeiro passo
     * >> Se método for POST, envia o e-mail com a senha temporária
     *
     * @return void
     * @see Admin_EsqueciMinhaSenhaController::_processIndex()
     *
     */
    public function indexAction()
    {
        $request = $this->getRequest();
        $f = new Admin_Form_EsqueciMinhaSenha();
		
        if($request->isPost()){
            $post = $request->getPost();
            
            if(!$f->isValid($post) || !$this->_processIndex($post['email'])) {
                $this->view->message = array("type"=>"erro","message"=>"E-mail inválido.");
				$f->populate($post);
            } else {
                $this->view->message = array("type"=>"","message"=>"Senha enviada! Acesse seu e-mail para receber a nova senha. - <a href=\"".URL."/admin/login/\">&laquo; Voltar para pág. de login</a><br/>");
            }
        }
        
		$this->view->form = $f->render();
    }
    
    /**
     * >> Verifica se o e-mail informado existe no banco de dados, caso exista
     *    cria uma senha temporária, cadastra no banco e envia para o e-mail.
     *
     * @access protected
     * @param String $_email
     * @return bool
     *
     */
    protected function _processIndex($_email)
    {
        $users = new Application_Model_Db_Usuario();
        
        if($user = $users->findByEmail($_email)){
            $temppass = $this->_genPass(6);
            $link = URL."/admin/login/";
            
            $html = "<h1>Olá ".$user->nome."</h1><hr/>".
                    "<p>".
                    "Você solicitou a recuperação de sua senha, a seguir segue senha temporária para acesso.<br/>".
                    "Lembre-se de alterá-la em seu primeiro acesso.<br/><br/>".
                    "Senha temporária: <b>".$temppass."</b><br/><br/>".
                    "<a href='".$link."'>Acessar página de login</a>".
                    "</p>".
                    "<p>Atenciosamente.</p>";
            $subject = 'Recuperação de senha';
            
            try {
                Trupe_BikiniMania_Mail::send($user->email,$user->nome,$subject,$html);
            } catch(Exception $e){
                $this->view->message = array("type"=>"erro","message"=>"Erro ao enviar e-mail. Tente novamente."/*$e->getMessage()*/);
            }
            
            //$user->ativo = 4;
            $user->senha = md5($temppass);
            $user->save();
            
            return true;
        }
        return false;
    }
    
    /**
     * Gera senha temporária
     */
    protected function _genPass($maxchars=10)
    {
        $alfa = "abcdefghijklmnopqrstuvwxyz";
        $num  = "0123456789";
        $hash = array();
        
        for($i=0;$i<$maxchars;$i++){
            $j = $i % 2 == 0 ? rand(0,(strlen($alfa)-1)) : rand(0,(strlen($num)-1));
            $hash[] = $i % 2 == 0 ? $alfa[$j] : $num[$j];
        }
        
        array_rand($hash);
        
        return implode("",$hash);
    }

    public function erroAction(){}
    
    public function okAction(){}
}

