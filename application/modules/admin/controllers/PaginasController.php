<?php

class Admin_PaginasController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "PÁGINAS";
        $this->view->section = $this->section = "paginas";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        // $this->tags = new Application_Model_Db_Tags();
        $this->fotos = new Application_Model_Db_Fotos();
        $this->paginas = new Application_Model_Db_Paginas();
        $this->fotos_fixas = new Application_Model_Db_PaginasFotosFixas();
        // $this->destaque = new Application_Model_Db_Destaque();
        $this->messenger = new Helper_Messenger();
        
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".FILE_PATH."/".$this->section;
        
        $this->view->MAX_FOTOS = 12;
    }

    public function indexAction()
    {
        $this->_redirect('admin/');
    }
    
    public function editAction()
    {
        $alias = $this->_getParam('alias');
        $table = new Application_Model_Db_Paginas();
        $row   = $table->fetchRow('alias = "'.$alias.'"');
        // _d($this->_hasParam('alias'));
        if(!$row){ $this->_forward('not-found','error','default',array('url'=>URL.'/admin/'));return false; }
        
        $parent= $table->fetchRow('id = '.(int)$row->parent_id);
        $form  = new Admin_Form_Paginas($row);
        
        $this->view->id           = $this->pagina_id = $row->id;
        $this->view->pagina       = $this->pagina = $row;
        $this->view->allow_photos = $row->allow_photos;
        $this->view->allow_photos2 = $row->allow_photos && $row->alias=='relacionamento';
        $this->view->allow_files  = $row->allow_files;
        $this->view->alias        = $row->alias;
        $this->view->titulo = $parent ?
                              Is_Str::toUpper(utf8_encode($parent->{'titulo_'.$this->view->lang}))." &rarr; ".Is_Str::toUpper(utf8_encode($row->{'titulo_'.$this->view->lang})):
                              $this->view->titulo." &rarr; ".Is_Str::toUpper(utf8_encode($row->{'titulo_'.$this->view->lang}));
        
        if($this->_request->isPost()){
            $post = $this->_request->getPost();
            
            if(!$form->isValid($post)){
                $this->messenger->addMessage("Preencha todos os campos corretamente.","erro");
            } else {
                try {
                    $row->body_pt = utf8_decode(Is_Str::nl2none(strip_tags($post['body_pt'],'<b><a><i><u><br><ul><ol><li><img><h3>')));
                    $row->body_en = utf8_decode(Is_Str::nl2none(strip_tags($post['body_en'],'<b><a><i><u><br><ul><ol><li><img><h3>')));

                    if($row->has_body2) $row->body2_pt = utf8_decode(Is_Str::nl2none(strip_tags($post['body2_pt'],'<b><a><i><u><br><ul><ol><li><img><h3>')));
                    if($row->has_body2) $row->body2_en = utf8_decode(Is_Str::nl2none(strip_tags($post['body2_en'],'<b><a><i><u><br><ul><ol><li><img><h3>')));
                    if($row->has_body3) $row->body3_pt = utf8_decode(Is_Str::nl2none(strip_tags($post['body3_pt'],'<b><a><i><u><br><ul><ol><li><img><h3>')));
                    if($row->has_body3) $row->body3_en = utf8_decode(Is_Str::nl2none(strip_tags($post['body3_en'],'<b><a><i><u><br><ul><ol><li><img><h3>')));
                    if($row->has_body4) $row->body4_pt = utf8_decode(Is_Str::nl2none(strip_tags($post['body4_pt'],'<b><a><i><u><br><ul><ol><li><img><h3>')));
                    if($row->has_body4) $row->body4_en = utf8_decode(Is_Str::nl2none(strip_tags($post['body4_en'],'<b><a><i><u><br><ul><ol><li><img><h3>')));

                    //$row->status_id = $post['status_id'];
                    $row->data_edit = date("Y-m-d H:i:s");
                    $row->user_edit = $this->login->user->id;
                    
                    $row->save();
                    $this->messenger->addMessage("Registro alterado com sucesso!","message");
                    $this->_redirect('admin/'.$this->section.'/edit/'.$row->alias.'/');
                } catch(Exception $e) {
                    $erro = strstr($e->getMessage(),"Duplicate") ?
                            "Já existe um registro semelhante, escolha outro nome." :
                            $e->getMessage();
                    $this->messenger->addMessage($erro,'erro');
                }
            }
        }
        
        $data = Is_Array::utf8DbRow($row);
        $data->{'body_'.$this->view->lang} = nl2br($data->{'body_'.$this->view->lang});
        $data->{'body2_'.$this->view->lang} = nl2br(@$data->{'body2_'.$this->view->lang});
        //Is_Var::dump((array)$data);

        $form->addElement('hidden','id');
        $form->populate((array)$data);
        
        $this->view->form = $form;
        $this->view->action = 'edit';
        
        $this->fotosAction();
        $this->arquivosAction();
        
        if(in_array($row->alias, array('como-fazemos','como-fazemos2'))){
            $this->etapasAction();
        }

        if($row->alias == 'quem-somos'){
            $this->clientesAction();
        }

        if(in_array($row->alias, array('quem-somos','cultura-de-trabalho','como-fazemos','o-que-fazemos'))){
            $this->view->allow_photos_fixas = true;
            $this->fotosFixasAction();
        }
    }

    public function saveFotosFixasAction()
    {
        if(!$this->_hasParam('fotos_fixas_id')) return $this->_forward('denied','error','default',array('url'=>URL.'/admin/'));
        
        $post = $this->_request->getParams();
        //$data_resposta = array_map('utf8_decode',$post['question']);
        //unset($post['question']);
        $data = array_map('utf8_decode',$post);
        $fotos = array();
        
        // remove dados desnecessários
        $unsets = 'submit,module,controller,action,fotos,enviar';
        foreach(explode(',',$unsets) as $u) if(isset($data[$u])) unset($data[$u]);
        
        // upload de arquivos
        $file = null; $rename = null;
        $table = $this->fotos_fixas;
        
        $check_uploads = array();
        // for($i=1;$i<=$data['fotos_fixas_qtde'];$i++) $check_uploads[] = 'foto'.$i;
        for($i=1;$i<=$data['fotos_fixas_qtde'];$i++) $check_uploads[] = 'foto'.$i.'_pt';
        for($i=1;$i<=$data['fotos_fixas_qtde'];$i++) $check_uploads[] = 'foto'.$i.'_en';
        $check_uploads = implode(',',$check_uploads);

        foreach(explode(',',$check_uploads) as $cu)
            if((bool)@$_FILES[$cu]) 
                if(!(bool)$_FILES[$cu]['name']) 
                    unset($_FILES[$cu]);
        
        foreach(explode(',',$check_uploads) as $cu) {
            if((bool)@$_FILES[$cu] && (bool)$_FILES[$cu]['name']){
                $file = $_FILES[$cu];
                $path = $this->img_path;
                $error = 'Imagem inválida';
                $ext = 'jpg,jpeg,png,bmp,gif,tiff';
            
                $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload->addValidator('Size', false, array('max' => '50mb'))
                       ->addValidator('Extension', false, $ext)
                       ->addFilter('Rename',$path.'/'.$rename,$cu)
                       ->setDestination($path);

                if($upload->isValid($cu)){
                    $upload->receive($cu);

                    $fotos[$cu] = $rename;
                } else {
                    $err = $error;
                    if(APPLICATION_ENV!='production') $err.= "<br/>\n".var_dump($upload->getErrors());
                    $this->messenger->addMessage($err,'error');
                    
                    $this->_redirect('admin/paginas/edit/'.$data['pagina_alias']);
                }
            }
        }
        
        if(count($fotos)) {
            $table->update($fotos,'id = '.$data['fotos_fixas_id']);
        }

        $this->messenger->addMessage('Fotos enviadas com sucesso');
        return $this->_redirect('admin/paginas/edit/'.$data['pagina_alias']);
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            $fotos->delete("id=".(int)$id);
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('paginas_fotos as tf')
            ->join('fotos as f','f.id=tf.foto_id')
            ->order('tf.id asc');
        
        if(isset($this->pagina_id)){
            $select->where('pagina_id = ?',$this->pagina_id);
        }
        
        $_fotos = $select->query()->fetchAll();
        
        array_walk($_fotos,'Func::_arrayToObject');
        
        $fotos = array(); $fotos2 = array();
        
        for($i=0;$i<sizeof($_fotos);$i++){
            in_array($_fotos[$i]->flag,array('g','2')) ?
                $fotos2[] = $_fotos[$i] :
                $fotos[] = $_fotos[$i];
        }
        //_d(array($fotos,$fotos2));
        $this->view->fotos = $fotos;
        $this->view->fotos2 = $fotos2;
    }

    public function fotosFixasAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('paginas_fotos_fixas as tf')
            ->order('tf.id asc');
        
        if(isset($this->pagina_id)){
            $select->where('pagina_id = ?',$this->pagina_id);
        }
        
        $_fotos = $select->query()->fetchAll();
        
        array_walk($_fotos,'Func::_arrayToObject');
        
        $fotos = array();
        
        for($i=0;$i<sizeof($_fotos);$i++){
            $fotos[] = $_fotos[$i];
        }
        
        //_d(array($fotos));
        $this->view->fotos_fixas = count($fotos) ? $fotos[0] : null;
    }

    public function etapasAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $table = new Application_Model_Db_Etapas();
        $where = isset($this->pagina_id) ? 'pagina_id = '.(int)$this->pagina_id : null;
        
        $rows = $table->fetchAll($where,'ordem');
        $rows = Is_Array::utf8DbResult($rows);

        /*foreach ($rows as &$row) {
            if((bool)$row->foto_id){
                $row->foto = $this->fotos->fetchRow('id='.$row->foto_id);
            }
        }*/

        $this->view->etapas = $rows;
    }
    
    public function uploadAction()
    {
        //echo $this->img_path;exit();
        
        if(!$this->_request->isPost()){
            return array('error'=>'Método não permitido.');
        }
        
        $file = $_FILES['file'];
        $isFile = $this->_hasParam('arquivo');
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Count', false, 1);
        
        if($isFile){
            $upload->addFilter('Rename',$this->file_path.'/'.$rename);
        } else {
            $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
                   ->addValidator('Size', false, array('max' => '5120kB'))
                   ->addFilter('Rename',$this->img_path.'/'.$rename);
        }
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até 5MB.');
        }
        
        try {
            $upload->receive();
            
            //$fotos
            $table  = $isFile ? new Application_Model_Db_Arquivos() : new Application_Model_Db_Fotos();
            //$paginas_fotos
            $table2 = $isFile ? new Application_Model_Db_PaginasArquivos() : new Application_Model_Db_PaginasFotos();
            $type   = $isFile ? "arquivo" : "foto";
            $pagina_id = $this->_getParam('id');
            
            //$data_fotos
            $data_insert = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s"),
                'flag'     => $this->_hasParam('flag') ?
                              $this->_getParam('flag') : null
            );
            
            if(!$insert_id = $table->insert($data_insert)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            
            $table2->insert(array(
                $type."_id" => $insert_id,
                "pagina_id" => $pagina_id
            ));
            
            return array("name"=>$rename,"id"=>$insert_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }
    
    public function arquivosDelAction()
    {
        $id = $this->_getParam("file");
        $arquivos = new Application_Model_Db_Arquivos();
        $arquivo = $arquivos->fetchRow('id='.(int)$id);
                
        try {
            $arquivos->delete("id=".(int)$id);
            Is_File::del($this->file_path.'/'.$arquivo->path);
            Is_File::delDerived($this->file_path.'/'.$arquivo->path);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function arquivosRenameAction()
    {
        $id = $this->_getParam("file");
        $descricao = $this->_getParam("descricao");
        $arquivos = new Application_Model_Db_Arquivos();
        $arquivo = $arquivos->fetchRow('id='.(int)$id);
                
        try {
            $arquivos->update(array('descricao'=>utf8_decode(urldecode($descricao))),"id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function arquivosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('paginas_arquivos as tf')
            ->join('arquivos as f','f.id=tf.arquivo_id')
            ->order('f.descricao asc');
        
        if(isset($this->pagina_id)){
            $select->where('pagina_id = ?',$this->pagina_id);
        }
        
        $arquivos = $select->query()->fetchAll();
        
        array_walk($arquivos,'Func::_arrayToObject');
        
        $this->view->arquivos = $arquivos;
    }
    
    public function linhadotempoDelAction()
    {
        $id = $this->_getParam("id");
        $linhas = new Application_Model_Db_Linhadotempo();
        $linha = $linhas->fetchRow('id='.(int)$id);
                
        try {
            $linhas->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function linhadotempoSaveAction()
    {
        $id = $this->_getParam("id");
        $params = $this->_request->getParams();
        $linhas = new Application_Model_Db_Linhadotempo();
        $linha = $linhas->fetchRow('id='.(int)$id);
        try {
            $data = array();
            $data['descricao'] = utf8_decode(urldecode($params['descricao']));
            $data['titulo']    = utf8_decode($params['ano']);
            $data['pagina_id'] = utf8_decode($params['pagina_id']);
            $data['user_edit'] = $this->login->user->id;
            $data['data_edit'] = date("Y-m-d H:i:s");
            
            if($linha){
                $linhas->update($data,'id = '.(int)$id);
            } else {
                $data['user_cad'] = $this->login->user->id;
                $data['data_cad'] = date("Y-m-d H:i:s");
                $linhas->insert($data);
            }
            
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function linhadotempoAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $table = new Application_Model_Db_Linhadotempo();
        $where = isset($this->pagina_id) ? 'pagina_id = '.(int)$this->pagina_id : null;
        
        $rows = $table->fetchAll($where,'id desc');
        $rows = Is_Array::utf8DbResult($rows);
        $this->view->linhadotempo = $rows;
        return $rows;
    }
    
    public function saveDescricaoAction()
    {
        if(!$this->_hasParam('pagina_id') ||
           !$this->_hasParam('foto_id') ||
           !$this->_hasParam('descricao')) {
            return array('error'=>'Acesso negado');
        }
        
        $f = new Application_Model_Db_Fotos();
        
        try{
            $f->update(
                array('descricao'=>$this->_getParam('descricao')),
                'id='.$this->_getParam('foto_id')
            );
            
            return array('msg'=>'Salvo.');
        } catch(Exception $e){
            return array('error'=>$e->getMessage());
        }
    }

    public function clientesAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $table = new Application_Model_Db_Clientes();
        $where = isset($this->pagina_id) ? 'pagina_id = '.(int)$this->pagina_id : null;
        
        $rows = $table->fetchAll($where,'id');
        $rows = Is_Array::utf8DbResult($rows);

        /*foreach ($rows as &$row) {
            if((bool)$row->foto_id){
                $row->foto = $this->fotos->fetchRow('id='.$row->foto_id);
            }
        }*/

        $this->view->clientes = $rows;
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }
}

