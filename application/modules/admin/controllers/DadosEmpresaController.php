<?php

class Admin_DadosEmpresaController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        Admin_Model_Login::checkAuth($this);
        
        $this->view->titulo = "DADOS DA EMPRESA";
        $this->view->section = $this->section = "dados-empresa";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path = $this->view->img_path = APPLICATION_PATH."/../..".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../..".FILE_PATH."/".$this->section;
        
        // models
        $this->dados = new Application_Model_Db_DadosEmpresa();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();
    }

    public function indexAction()
    {
        $this->_redirect('admin/dados-empresa/edit/1/');
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; NOVO");
        $form = new Admin_Form_DadosEmpresa();
        
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            $this->view->id = $this->usuario_id = $data['id'];
            $form->addElement('hidden','id');
        } else {
            $data = array('status_id'=>'1');
        }
        
        $form->populate($data);
        $this->view->form = $form;
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url.'new/'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->dados->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $data = $this->_request->getParams();
            $data['endereco_pt'] = cleanHtml($data['endereco_pt']);
            $data['endereco_en'] = cleanHtml($data['endereco_en']);
            $data['bairro_pt'] = cleanHtml($data['bairro_pt']);
            $data['bairro_en'] = cleanHtml($data['bairro_en']);
            $data['cidade'] = cleanHtml($data['cidade']);
            $data['estado'] = cleanHtml($data['estado']);
            $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
            $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
            $data = array_map('utf8_decode',$data);
            
            // clean
            $cleans = 'cep';//,tel1,tel2,tel3,tel4';
            foreach(explode(',',$cleans) as $c) $data[$c] = Is_Cpf::clean($data[$c]);

            if(($row && $data['senha'] != '') || !$row){
                $data['senha'] = md5($data['senha']);
            } else {
                unset($data['senha']);
            }
            
            // remove dados desnecessários
            if(isset($data['submit'])){ unset($data['submit']); }
            if(isset($data['module'])){ unset($data['module']); }
            if(isset($data['controller'])){ unset($data['controller']); }
            if(isset($data['action'])){ unset($data['action']); }
            
            ($row) ? $this->dados->update($data,'id='.$id) : $id = $this->dados->insert($data);
            
            $this->messenger->addMessage('Registro atualizado.');
            $data['id'] = $id;
            $this->_redirect('admin/'.$this->section.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>Is_Array::utf8All($data)));
        } catch(Exception $e) {
            $this->messenger->addMessage($e->getMessage(),'error');
            $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->dados->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('not-found','error','default',array('url'=>$this->_url));return false; }
        $this->_forward('new',null,null,array('data'=>Is_Array::utf8All($row->toArray())));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        
        try {
            $this->dados->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }
}