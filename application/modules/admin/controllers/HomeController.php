<?php

class Admin_HomeController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        Application_Model_Login::checkAuth($this);

        $this->view->titulo = "HOME";
        $this->view->section = $this->section = "home";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".FILE_PATH."/".$this->section;
        
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();
        $this->form = new Admin_Form_Home();
        $this->pagina_home = new Application_Model_Db_PaginaHome();
        $this->pagina_home_fotos = new Application_Model_Db_PaginaHomeFotos();
        $this->fotos_fixas = new Application_Model_Db_PaginaHome();
        
        $this->view->MAX_FOTOS = 15;

        // Admin_Model_Login::checkAuth($this,$this->section) ||
            // $this->_forward('denied','error','default',array('url'=>URL.'/admin'));
        $this->view->id = $this->produto_id = $this->pagina_id = 1;
    }

    public function indexAction()
    {
        $form = $this->form;
        $row = $this->pagina_home->fetchRow('id='.$this->pagina_id);
        
        if($row) $form->populate(array(
            // 'titulo_pt' => utf8_encode($row->titulo_pt),
            // 'titulo_en' => utf8_encode($row->titulo_en),
            'texto1_pt' => utf8_encode($row->texto1_pt),
            'texto1_en' => utf8_encode($row->texto1_en),
            // 'texto2_pt' => utf8_encode($row->texto2_pt)
            // 'texto2_en' => utf8_encode($row->texto2_en)
            // 'video' => utf8_encode($row->video),
            'video_pt' => utf8_encode($row->video_pt),
            'video_en' => utf8_encode($row->video_en),
            'video_titulo_pt' => utf8_encode($row->video_titulo_pt),
            'video_titulo_en' => utf8_encode($row->video_titulo_en),
        ));
        
        $this->view->form = $form;
        $this->view->fotos = $this->fotosAction(1);
        // $this->view->fotos2 = $this->fotosAction(2);

        $this->view->allow_photos_fixas = true;
        $this->view->fotos_fixas = $row;
    }
    
    public function saveAction()
    {
        $id = $this->pagina_id;
        $params = $this->_request->getParams();
        $row = $this->pagina_home->fetchRow('id='.(int)$id);
        
        try {
            $data = array();
            // $data['video']    = utf8_decode($params['video']);
            $data['video_pt']    = utf8_decode($params['video_pt']);
            $data['video_en']    = utf8_decode($params['video_en']);
            $data['video_titulo_pt'] = utf8_decode($params['video_titulo_pt']);
            $data['video_titulo_en'] = utf8_decode($params['video_titulo_en']);
            // $data['titulo']    = utf8_decode($params['titulo']);
            $data['texto1_pt']    = utf8_decode((strip_tags($params['texto1_pt'],'<b><a><i><u><br><ul><ol><li><img><h3>')));
            $data['texto1_en']    = utf8_decode((strip_tags($params['texto1_en'],'<b><a><i><u><br><ul><ol><li><img><h3>')));
            // $data['texto2']    = utf8_decode($params['texto2']);
            $data['user_edit'] = $this->login->user->id;
            $data['data_edit'] = date("Y-m-d H:i:s");
            
            if($row){
                $this->pagina_home->update($data,'id = '.(int)$id);
            } else {
                $data['user_cad'] = $this->login->user->id;
                $data['data_cad'] = date("Y-m-d H:i:s");
                $id = $this->pagina_home->insert($data);
            }
            
            //return array("id"=>$id);
            $this->messenger->addMessage('Página atualizada.');
            $this->_redirect('admin/'.$this->section);
        } catch(Exception $e) {
            $this->messenger->addMessage($e->getMessage(),'error');
            $this->_redirect('admin/'.$this->section);
        }
    }

    public function saveFotosFixasAction()
    {
        if(!$this->_hasParam('fotos_fixas_id')) return $this->_forward('denied','error','default',array('url'=>URL.'/admin/'));
        
        $post = $this->_request->getParams();
        //$data_resposta = array_map('utf8_decode',$post['question']);
        //unset($post['question']);
        $data = array_map('utf8_decode',$post);
        $fotos = array();
        
        // remove dados desnecessários
        $unsets = 'submit,module,controller,action,fotos,enviar';
        foreach(explode(',',$unsets) as $u) if(isset($data[$u])) unset($data[$u]);
        
        // upload de arquivos
        $file = null; $rename = null;
        $table = $this->fotos_fixas;
        
        $check_uploads = array();
        for($i=1;$i<=$data['fotos_fixas_qtde'];$i++) $check_uploads[] = 'foto'.$i.'_pt';
        for($i=1;$i<=$data['fotos_fixas_qtde'];$i++) $check_uploads[] = 'foto'.$i.'_en';
        $check_uploads = implode(',',$check_uploads);

        foreach(explode(',',$check_uploads) as $cu)
            if((bool)@$_FILES[$cu]) 
                if(!(bool)$_FILES[$cu]['name']) 
                    unset($_FILES[$cu]);
        
        foreach(explode(',',$check_uploads) as $cu) {
            if((bool)@$_FILES[$cu] && (bool)$_FILES[$cu]['name']){
                $file = $_FILES[$cu];
                $path = $this->img_path;
                $error = 'Imagem inválida';
                $ext = 'jpg,jpeg,png,bmp,gif,tiff';
            
                $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload->addValidator('Size', false, array('max' => '50mb'))
                       ->addValidator('Extension', false, $ext)
                       ->addFilter('Rename',$path.'/'.$rename,$cu)
                       ->setDestination($path);

                if($upload->isValid($cu)){
                    $upload->receive($cu);

                    $fotos[$cu] = $rename;
                } else {
                    $err = $error;
                    if(APPLICATION_ENV!='production') $err.= "<br/>\n".var_dump($upload->getErrors());
                    $this->messenger->addMessage($err,'error');
                    
                    $this->_redirect('admin/'.$this->section);
                }
            }
        }
        
        if(count($fotos)) {
            $table->update($fotos,'id = '.$data['fotos_fixas_id']);
        }

        $this->messenger->addMessage('Fotos enviadas com sucesso');
        return $this->_redirect('admin/'.$this->section);
    }
    
    public function fotosAction($tipo=1)
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('pagina_home_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->where('f2.tipo = '.$tipo)
            ->order('f2.id asc');
        
        if(isset($this->produto_id)){
            $select->where('f2.pagina_id = ?',$this->produto_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            $fotos->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        //echo $this->img_path;exit();
        $max_size = '5120'; // '2048'
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/'.$this->section));
            return;
        }
        
        $file = $_FILES['file'];
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
               ->addValidator('Size', false, array('max' => $max_size.'kb'))
               ->addValidator('Count', false, 1)
               ->addFilter('Rename',$this->img_path.'/'.$rename)
               ->setDestination($this->img_path);
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.Is_File::formatBytes($max_size).'.');
        }
        
        try {
            $upload->receive();
            
            $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
            $thumb->resize('1000','1000');
            $thumb->save($this->img_path.'/'.$rename);
            
            $fotos = new Application_Model_Db_Fotos();
            $produtos_fotos = new Application_Model_Db_PaginaHomeFotos();
            $produto_id = $this->_getParam('id');
            
            $data_fotos = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$foto_id = $fotos->insert($data_fotos)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            $produtos_fotos->insert(array(
                "foto_id"=>$foto_id,
                "pagina_id"=>$produto_id,
                "tipo"=>($this->_hasParam('t')?$this->_getParam('t'):1)
            ));
            
            return array("name"=>$rename,"id"=>$foto_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }

    public function saveAllAction()
    {
        if(!$this->_hasParam('id')) {
            return array('error'=>'Acesso negado');
        }
        
        $f = new Application_Model_Db_Fotos();
        $post = $this->_request->getParams();
        $id = $post['id'];

        // limpando dados
        $limpar = array('module','controller','action','portfolio_id','id');
        foreach($limpar as $l) if(isset($post[$l])) unset($post[$l]);
        foreach($post as $k=>$v){
            $post[$k] = utf8_decode($v);
            if($v=='__none__') $post[$k] = null;
        }
        
        if(empty($post)) return array('error'=>'Preencha os campos');

        try{
            $f->update($post,'id='.$id);
            
            return array('msg'=>'Salvo.');
        } catch(Exception $e){
            return array('error'=>$e->getMessage());
        }
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }
}

