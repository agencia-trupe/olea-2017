<?php

class Admin_Model_Login
{
    public function checkAuth(&$c,$role=null){
        $login = new Zend_Session_Namespace(SITE_NAME."_login");
        
        if (!Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session(SITE_NAME))->hasIdentity() ||
            !isset($login->acl) ||
            @$login->acl != SITE_NAME) {
            self::setRedirectUrl($c);
			return self::logout($c);
			//return self::redirect($c);
        } else {
            $acl = new Zend_Acl();
            
            // acl roles
            $acl->addRole(new Zend_Acl_Role(SITE_NAME))->add(new Zend_Acl_Resource(SITE_NAME))->allow(SITE_NAME,SITE_NAME);
            
            $acl->addRole(new Zend_Acl_Role('0')) // super admin
                ->addRole(new Zend_Acl_Role('1')) // admin
                ->addRole(new Zend_Acl_Role('2')) // usuario
                ->addRole(new Zend_Acl_Role('3')) // 
                ->addRole(new Zend_Acl_Role('4')); // relatório
            
            // acl resources allow / deny
            $allows = array(
                'home' => '0,1',
                'produtos' => '0,1',
                'noticias' => '0,1',
                'parceiros' => '0,1',
                'usuarios' => '0,1',
                'categorias' => '0,1',
                'paginas' => '0,1',
                'legislacao' => '0,1',
                'formacao' => '0,1',
            );

            foreach($allows as $k => $val){
                $acl->add(new Zend_Acl_Resource($k));
                $va = explode(',',$val);
                foreach($va as $v) $acl->allow($v,$k);
            }

            if(!$acl->isAllowed($login->acl,SITE_NAME)){
                self::setRedirectUrl($c);
                return self::logout($c);
            }
            
            if($role) return $acl->isAllowed(
                $login->user->role,
                is_bool($role) ?
                    $c->view->controller.'_'.$c->view->action :
                    $role
            );

            /*$acl = new Zend_Acl();
            $acl->addRole(new Zend_Acl_Role(SITE_NAME))->add(new Zend_Acl_Resource(SITE_NAME))->allow(SITE_NAME,SITE_NAME);
            
            if(!$acl->isAllowed($login->acl,SITE_NAME)){
                self::setRedirectUrl($c);
                return self::logout($c);
            }*/
        }
    }
    
    public function logout(&$c)
    {
        Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session(SITE_NAME))->clearIdentity();
        Zend_Session::regenerateId();
        //self::unsetRedirectUrl();
		//Zend_Session::destroy();
		return self::redirect($c);
    }
    
    public function redirect(&$c)
    {
        $moduleName = $c->getRequest()->getModuleName();
		$controller = $c->getRequest()->getControllerName();
		$action     = $c->getRequest()->getActionName();
        
        return $c->getHelper("redirector")->goToRoute(array(
            'controller' => 'login',
            'module' => $moduleName,
            'action' => 'index'
        ), 'default', true);
    }
    
    /**
     * Adaptador do Objeto Zend_Auth
     */
    public function _getAuthAdapter()
    {
		$dbAdapter = Zend_Db_Table::getDefaultAdapter();
		$authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
		
		$authAdapter->setTableName('usuarios')
			->setIdentityColumn('login')
			->setCredentialColumn('senha')
			->setCredentialTreatment('MD5(?)');
			
		return $authAdapter;
	}
    
    /**
     * GETTERS / SETTERS Session Redirect Url
     */
    public function setRedirectUrl(&$c)
    {
        $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url_login = URL.'/'.$c->getRequest()->getModuleName().'/login';
        
        $_SESSION[SITE_NAME.'_REDIRECT_TO'] = ($url != $url_login) ? $url : null;
    }
    
    public function getRedirectUrl()
    {
        return isset($_SESSION[SITE_NAME.'_REDIRECT_TO']) ? $_SESSION[SITE_NAME.'_REDIRECT_TO'] : null;
    }
    
    public function unsetRedirectUrl()
    {
        $_SESSION[SITE_NAME.'_REDIRECT_TO'] = null;
    }
    
    public function isLogged()
    {
        return Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session(SITE_NAME))->hasIdentity();
    }
}
