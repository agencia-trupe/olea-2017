<?php

class Admin_Form_Portfolio extends ZendPlugin_Form
{
    public function init()
    {
		$section = 'projetos';
		
        // configurações
        $this->setMethod('post')->setAction(URL.'/admin/'.$section.'/save')
             ->setAttrib('id','frm-'.$section.'')
             ->setAttrib('name','frm-'.$section.'');
		
        // elementos
        $this->addElement('text','titulo_pt',array('label'=>'Título (pt)','class'=>'txt'));
        $this->addElement('text','titulo_en',array('label'=>'Título (en)','class'=>'txt'));
        $this->addElement('text','cliente',array('label'=>'Cliente','class'=>'txt'));
        $this->addElement('text','ano',array('label'=>'Ano','class'=>'txt mask-int','maxlength'=>4));
        $this->addElement('textarea','descricao_pt',array('label'=>'Descrição (pt)','class'=>'txt'));
        $this->addElement('textarea','descricao_en',array('label'=>'Descrição (en)','class'=>'txt'));
        $this->addElement('checkbox','destaque',array('label'=>'Destaque'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        //$this->getElement('descricao')->setAttrib('rows',1)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo_pt')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

