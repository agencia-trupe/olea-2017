<?php

class Admin_Form_BlogsComments extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/blogs-comments/save')
             ->setAttrib('id','frm-noticias')
             ->setAttrib('name','frm-noticias');
        
        // elementos
        // $this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));
        // $this->addElement('text','data',array('label'=>'Data','class'=>'txt mask-date'));
        // $this->addElement('hidden','alias');
        //$this->addElement('checkbox','allow_files',array('label'=>'Arquivos'));
        //$this->addElement('checkbox','allow_photos',array('label'=>'Inserir imagens?'));
        $this->addElement('textarea','comentario',array('label'=>'Comentário','class'=>'txt'));
        $this->addElement('checkbox','status',array('label'=>'Ativo'));
        
        // atributos
        $this->getElement('comentario')->setAttrib('rows',15)->setAttrib('cols',1);
        
        // filtros / validações
        // $this->getElement('titulo')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

