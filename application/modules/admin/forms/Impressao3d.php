<?php

class Admin_Form_Impressao3d extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/impressao3d/save')
             ->setAttrib('id','frm-home')
             ->setAttrib('name','frm-home');
        
        // elementos
        // $this->addElement('text','titulo',array('label'=>'Título:','class'=>'txt'));
        // $this->addElement('text','video',array('label'=>'Vídeo:','class'=>'txt'));
        $this->addElement('text','video_pt',array('label'=>'Vídeo (pt):','class'=>'txt'));
        $this->addElement('text','video_en',array('label'=>'Vídeo (en):','class'=>'txt'));
        $this->addElement('textarea','texto1_pt',array('label'=>'Texto (pt):','class'=>'txt wysiwyg'));
        $this->addElement('textarea','texto1_en',array('label'=>'Texto (en):','class'=>'txt wysiwyg'));
        // $this->addElement('textarea','texto2',array('label'=>'Texto 2:','class'=>'txt wysiwyg'));
        
        // atributos
        $this->getElement('texto1_pt')->setAttrib('rows',10);
        $this->getElement('texto1_en')->setAttrib('rows',10);
        // $this->getElement('texto2')->setAttrib('rows',10);
        
        // filtros / validações
        // $this->getElement('video')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}