<?php

class Admin_Form_Paginas extends Zend_Form
{
    public $row = null;

    public function __construct($row=null)
    {
        $this->row = $row;
        parent::__construct();
    }

    public function init()
    {
        $this->setMethod('post')->setAction('')->setAttrib('id','frm-pousada')->setAttrib('name','frm-pousada');
		
		// $this->addElement('textarea','body_pt',array('label'=>'Conteúdo (pt):','class'=>'txt'));
		$this->addElement('hidden','alias',array('class'=>'txt'));
        //$this->addElement('text','alias',array('label'=>'Alias:','class'=>'txt alias disabled','disabled'=>true));
        $this->addElement('textarea','body_pt',array('class'=>'txt wysiwyg'));
        $this->addElement('textarea','body_en',array('class'=>'txt wysiwyg'));
		
        if(@$this->row->has_body2) $this->addElement('textarea','body2_pt',array('class'=>'txt wysiwyg'));
        if(@$this->row->has_body2) $this->addElement('textarea','body2_en',array('class'=>'txt wysiwyg'));
        if(@$this->row->has_body3) $this->addElement('textarea','body3_pt',array('class'=>'txt wysiwyg'));
        if(@$this->row->has_body3) $this->addElement('textarea','body3_en',array('class'=>'txt wysiwyg'));
        if(@$this->row->has_body4) $this->addElement('textarea','body4_pt',array('class'=>'txt wysiwyg'));
        if(@$this->row->has_body4) $this->addElement('textarea','body4_en',array('class'=>'txt wysiwyg'));

		//$this->addElement('checkbox','status_id',array('label'=>'Ativo:','class'=>'','checked'=>true));
        $this->addElement('submit','submit',array('label'=>'Salvar','class'=>'bt'));
        
        //$this->getElement('status_id')->setRequired();
        $this->getElement('body_pt')->setAttrib('rows','5')->setAttrib('cols','103');
        $this->getElement('body_en')->setAttrib('rows','5')->setAttrib('cols','103');
        if(@$this->row->has_body2) $this->getElement('body2_pt')->setAttrib('rows','5')->setAttrib('cols','103');
        if(@$this->row->has_body2) $this->getElement('body2_en')->setAttrib('rows','5')->setAttrib('cols','103');
        if(@$this->row->has_body3) $this->getElement('body3_pt')->setAttrib('rows','5')->setAttrib('cols','103');
        if(@$this->row->has_body3) $this->getElement('body3_en')->setAttrib('rows','5')->setAttrib('cols','103');
        if(@$this->row->has_body4) $this->getElement('body4_pt')->setAttrib('rows','5')->setAttrib('cols','103');
        if(@$this->row->has_body4) $this->getElement('body4_en')->setAttrib('rows','5')->setAttrib('cols','103');
        
        $this->removeDecs(array('label','htmlTag','description','errors'));
    }
    
    public function removeDecs($decorators = array('label','htmlTag','description','errors'),$elms=array())
    {
        $_elms = &$this->getElements();
        $elms = count($elms) ? $elms : $_elms;
        foreach($elms as $elm){
            foreach($decorators as $decorator){
                $elm->removeDecorator($decorator);
            }
        }
        return $this;
    }
}