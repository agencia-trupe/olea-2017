<?php

class Admin_Form_IdeacaoPassos extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/ideacao-passos/save/')
             ->setAttrib('id','frm-noticias')
             ->setAttrib('name','frm-noticias');
        
        // elementos
        $this->addElement('text','ordem',array('label'=>'Ordem #','class'=>'txt mask-int','maxlength'=>2));
        $this->addElement('text','titulo_pt',array('label'=>'Título (pt)','class'=>'txt'));
        $this->addElement('text','titulo_en',array('label'=>'Título (en)','class'=>'txt'));
        $this->addElement('hidden','alias');
        $this->addElement('hidden','pagina_id');
        // $this->addElement('select','perfil_id',array('label'=>'Perfil','class'=>'txt','multiOptions'=>perfilKeyValues()));
        // $this->addElement('select','lang',array('label'=>'Idioma','class'=>'txt','multiOptions'=>langKeyValues()));
        //$this->addElement('checkbox','allow_files',array('label'=>'Arquivos'));
        //$this->addElement('checkbox','allow_photos',array('label'=>'Inserir imagens?'));
        // $this->addElement('textarea','olho',array('label'=>'Olho','class'=>'txt'));
        $this->addElement('textarea','body_pt',array('label'=>'Conteúdo (pt)','class'=>'txt wysiwyg'));
        $this->addElement('textarea','body_en',array('label'=>'Conteúdo (en)','class'=>'txt wysiwyg'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        $this->getElement('body_pt')->setAttrib('rows',10)->setAttrib('cols',1);
        $this->getElement('body_en')->setAttrib('rows',10)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo_pt')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

