<?php

class Admin_Form_BlogsPosts extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/blogs-posts/save/')
             ->setAttrib('id','frm-noticias')
             ->setAttrib('name','frm-noticias');
        
        $categs = new Application_Model_Db_CategoriasBlog();

        // elementos
        $this->addElement('select','categoria_id',array('label'=>'Categoria','class'=>'txt','multiOptions'=>$categs->getKeyValues('descricao_pt')));
        $this->addElement('text','titulo_pt',array('label'=>'Título (pt)','class'=>'txt'));
        $this->addElement('text','titulo_en',array('label'=>'Título (en)','class'=>'txt'));
        // $this->addElement('text','data',array('label'=>'Data','class'=>'txt mask-date'));
        $this->addElement('hidden','alias');
        //$this->addElement('checkbox','allow_files',array('label'=>'Arquivos'));
        //$this->addElement('checkbox','allow_photos',array('label'=>'Inserir imagens?'));
        $this->addElement('textarea','body_pt',array('label'=>'Conteúdo (pt)','class'=>'txt wysiwyg'));
        $this->addElement('textarea','body_en',array('label'=>'Conteúdo (en)','class'=>'txt wysiwyg'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        $this->getElement('body_pt')->setAttrib('rows',15)->setAttrib('cols',1);
        $this->getElement('body_en')->setAttrib('rows',15)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo_pt')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

