<?php

class Admin_Form_Home extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/home/save')
             ->setAttrib('id','frm-home')
             ->setAttrib('name','frm-home');
        
        // elementos
        // $this->addElement('text','titulo',array('label'=>'Título:','class'=>'txt'));
        // $this->addElement('textarea','texto2',array('label'=>'Texto 2:','class'=>'txt'));
        // $this->addElement('text','video',array('label'=>'Vídeo:','class'=>'txt'));
        $this->addElement('text','video_titulo_pt',array('label'=>'Vídeo Título (pt):','class'=>'txt'));
        $this->addElement('text','video_titulo_en',array('label'=>'Vídeo Título (en):','class'=>'txt'));
        $this->addElement('text','video_pt',array('label'=>'Vídeo URL (pt):','class'=>'txt'));
        $this->addElement('text','video_en',array('label'=>'Vídeo URL (en):','class'=>'txt'));
        $this->addElement('textarea','texto1_pt',array('label'=>'Texto (pt):','class'=>'txt wysiwyg'));
        $this->addElement('textarea','texto1_en',array('label'=>'Texto (en):','class'=>'txt wysiwyg'));
        
        // atributos
        // $this->getElement('titulo')->setAttrib('rows',15);
        $this->getElement('texto1_pt')->setAttrib('rows',15)->setAttrib('cols',1);
        $this->getElement('texto1_en')->setAttrib('rows',15)->setAttrib('cols',1);
        
        // filtros / validações
        // $this->getElement('video')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}