<?php

class Impressao3dController extends ZendPlugin_Controller_Ajax
{
    private $pagina_id = 2;

    public function init()
    {
        $this->paginas = new Application_Model_Db_PaginaHome();
        $this->caracteristicas = new Application_Model_Db_Caracteristicas();
        $this->impressao3d_arquivos = new Application_Model_Db_Impressao3dArquivos();
        $this->arquivos = new Application_Model_Db_Arquivos();
        $this->messenger = new Helper_Messenger();

        $this->view->section = $this->section = "impressao-3d";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/".$this->section."/";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".FILE_PATH."/".$this->section;
    }

    public function indexAction()
    {
        $pagina = Is_Array::utf8DbRow(
            $this->paginas->fetchRow('id = '.$this->pagina_id)
        );

        $pagina->fotos = $this->paginas->getFotos($pagina->id);
        
        $titulo = 'titulo_'.$this->view->lang;
        $this->view->pagina = $pagina;
        $this->view->titulo = $pagina->{$titulo};
        
        $this->view->caracteristicas = $this->paginas->getCaracteristicas($pagina->id,false);
    }

    public function enviarAction()
    {
        // envio
        $r = $this->getRequest();
        
        if($r->isPost()){
            $form = new Application_Form_Impressao3d();
            $post = $r->getPost();
            
            if($form->isValid($post)){ // valida post
                // upload do curriculo
                $file = null; $rename = null;

                if((bool)@$_FILES['arquivo']){
                    $file = $_FILES['arquivo'];

                    $v = array( // validações
                        'ext' => 'stl,iges',
                        'size' => '100mb'
                    );

                    $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
                    $upload = new Zend_File_Transfer_Adapter_Http();
                    $upload->addValidator('Extension', false, $v['ext'])
                           ->addValidator('Size', false, array('max' => $v['size']))
                           ->addValidator('Count', false, 1)
                           ->addFilter('Rename',$this->file_path.'/'.$rename)
                           ->setDestination($this->file_path);
                    
                    if(!$upload->isValid()){
                        $this->messenger->addMessage('O arquivo deve possuir até '.$v['size'].'<br/> e ter uma das extensões a seguir: '.$v['ext'].'.','error');
                        return $this->_redirect('impressao-3d');
                    }
                }

                // cadastra no banco
                $data = Is_Array::deUtf8All($post);
                $data['tel'] = $r->getParam('ddd').Is_Cpf::clean($data['tel']);

                // unsets
                $unsets = 'controller,action,module,ddd';
                foreach(explode(',',$unsets) as $u) if(isset($data[$u])) unset($data[$u]);

                // envia email
                $html = "<h1>Impressão 3D</h1>". // monta html
                        "Novo arquivo enviado<br/><br/>".
                        "<b>Nome:</b> ".$r->getParam('nome')."<br/>".
                        "<b>E-mail:</b> <a href='mailto:".
                        $r->getParam('email')."'>".$r->getParam('email').
                        "</a><br/>".
                        "<b>Telefone:</b> ".$r->getParam('ddd')." ".$r->getParam('tel')."<br/>".
                        "<b>Mensagem:</b> ".$r->getParam('mensagem');
                
                try { // tenta enviar o e-mail e cadastrar no banco
                    if($file){ // upload final e cadastro de curriculo
                        $upload->receive();

                        $data['arquivo_id'] = $this->arquivos->insert(array(
                            "path"     => $rename,
                            "data_cad" => date("Y-m-d H:i:s")
                        ));

                        $c_url = URL.'/public/files/'.$this->section.'/'.$rename;

                        $html.= '<br/><br/>'.
                                '<a href="'.$c_url.'">Visualizar arquivo</a>';
                    }

                    $this->impressao3d_arquivos->insert($data);

                    if(APPLICATION_ENV!='development') Trupe_Olea_Mail::sendWithReplyRH(
                        $post['email'],
                        $post['nome'],
                        'Impressão 3D Studio Olea',
                        $html
                    );

                    $this->messenger->addMessage('Arquivo enviado com <b>sucesso</b>! <br/>'.
                                                 'Entraremos em contato em breve.','return');
                    /*$this->messenger->addMessage('Formulário enviado!<br/>'.
                                        'Em breve retornaremos o contato.');*/
                } catch(Exception $e){
                    $err = strstr($e->getMessage(),'uplicate')?'Você já enviou este arquivo.':'Erro ao enviar formulário.';
                    $err.= (APPLICATION_ENV=='development')?'<br/>'.$e->getMessage():'';
                    $this->messenger->addMessage($err,'error');
                }

                return $this->_redirect('impressao-3d');
            }
            
            $this->messenger->addMessage("Preencha todos os campos",'error');
            return $this->_redirect('impressao-3d');
        }
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
    }
    
}