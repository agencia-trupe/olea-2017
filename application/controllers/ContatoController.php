<?php

class ContatoController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        $this->vagas = new Application_Model_Db_Vagas();
        $this->curriculos = new Application_Model_Db_VagasCurriculos();
        $this->arquivos = new Application_Model_Db_Arquivos();
        $this->messenger = new Helper_Messenger();

        $this->view->section = $this->section = "faca-parte";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/".$this->section."/";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".FILE_PATH."/".$this->section;
    }

    public function indexAction()
    {
        $this->view->titulo = $this->view->translate('contato');
    }
    
    public function enviarAction()
    {
        $r = $this->getRequest();
        
        if(!$r->isPost()){ // interrompe se não for Post
            return array('error'=>$this->view->translate('error_request'));
        }
        
        $form = new Application_Form_Contato();
        $post = $r->getPost();
        $assunto = (bool)trim($r->getParam('assunto')) ? ' - '.trim($r->getParam('assunto')) : '';
        
        if($form->isValid($post)){ // valida post
            $html = "<h1>Contato".$assunto."</h1>". // monta html
                    nl2br($r->getParam('mensagem'))."<br/><br/>".
                    "<b>Nome:</b> ".$r->getParam('nome')."<br/>".
                    "<b>E-mail:</b> <a href='mailto:".
                    $r->getParam('email')."'>".$r->getParam('email').
                    "</a><br/>".
                    "<b>Telefone:</b> ".$r->getParam('ddd')." ".$r->getParam('tel');
            
            // unsets
            $unsets = 'controller,action,module,ddd';
            foreach(explode(',',$unsets) as $u) if(isset($post[$u])) unset($post[$u]);

            try { // tenta enviar o e-mail
                if(APPLICATION_ENV!='development1') Trupe_Olea_Mail::sendWithReply(
                    $post['email'],
                    $post['nome'],
                    'Contato'.$assunto,
                    $html
                );
                return array('msg'=>$this->view->translate('campo_status_ok'));
            } catch(Exception $e){
                return array('error'=>$e->getMessage());
            }
        }
        
        return array('error'=>$this->view->translate('campo_status_preencha'));
    }

    public function facaParteAction()
    {
        $this->view->titulo = 'Faça parte';

        /*$vagas = $this->vagas->getVagas('status_id=1',array('setor_id','titulo'));
        $vagas_combo = array();

        if(count($vagas))
            foreach ($vagas as $v) 
                $vagas_combo[$v->id] = $v->setor->titulo.' - '.$v->titulo;

        $this->view->vagas = $vagas;
        $this->view->vagas_combo = array(''=>'Selecione')+$vagas_combo;*/

        // envio
        $r = $this->getRequest();
        
        if($r->isPost()){
            $form = new Application_Form_FacaParte();
            $post = $r->getPost();
            
            if($form->isValid($post)){ // valida post
                // upload do curriculo
                $file = null; $rename = null;

                if((bool)@$_FILES['arquivo']){
                    $file = $_FILES['arquivo'];

                    $v = array( // validações
                        'ext' => 'doc,docx,pdf,odt,rtf',
                        'size' => '10mb'
                    );

                    $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
                    $upload = new Zend_File_Transfer_Adapter_Http();
                    $upload->addValidator('Extension', false, $v['ext'])
                           ->addValidator('Size', false, array('max' => $v['size']))
                           ->addValidator('Count', false, 1)
                           ->addFilter('Rename',$this->file_path.'/'.$rename)
                           ->setDestination($this->file_path);
                    
                    if(!$upload->isValid()){
                        $this->messenger->addMessage('O arquivo deve possuir até '.$v['size'].'<br/> e ter uma das extensões a seguir: '.$v['ext'].'.','error');
                        return $this->_redirect('contato');
                    }
                }

                // cadastra no banco
                $data = Is_Array::deUtf8All($post);
                $data['tel'] = $r->getParam('ddd').Is_Cpf::clean($data['tel']);

                // unsets
                $unsets = 'controller,action,module,ddd';
                foreach(explode(',',$unsets) as $u) if(isset($data[$u])) unset($data[$u]);

                // envia email
                $html = "<h1>Faça Parte da Olea</h1>". // monta html
                        // "Novo currículo para vaga de <b>".$vagas_combo[$data['vaga_id']]."</b><br/><br/>".
                        "Novo currículo enviado<br/><br/>".
                        "<b>Nome:</b> ".$r->getParam('nome')."<br/>".
                        "<b>E-mail:</b> <a href='mailto:".
                        $r->getParam('email')."'>".$r->getParam('email').
                        "</a><br/>".
                        "<b>Portifólio:</b> <a href='".
                        $r->getParam('portfolio')."'>".$r->getParam('portfolio').
                        "</a><br/>".
                        "<b>Telefone:</b> ".$r->getParam('ddd')." ".$r->getParam('tel')."<br/>".
                        "<b>Mensagem:</b> ".$r->getParam('mensagem');
                
                try { // tenta enviar o e-mail e cadastrar no banco
                    if($file){ // upload final e cadastro de curriculo
                        $upload->receive();

                        $data['arquivo_id'] = $this->arquivos->insert(array(
                            "path"     => $rename,
                            "data_cad" => date("Y-m-d H:i:s")
                        ));

                        $c_url = URL.'/public/files/'.$this->section.'/'.$rename;

                        $html.= '<br/><br/>'.
                                '<a href="'.$c_url.'">Visualizar currículo</a>';
                    }

                    $this->curriculos->insert($data);

                    if(APPLICATION_ENV!='development1') Trupe_Olea_Mail::sendWithReplyRH(
                        $post['email'],
                        $post['nome'],
                        'Faça parte do Studio Olea',
                        $html
                    );

                    $this->messenger->addMessage($this->view->translate('campo_status_ok'));
                } catch(Exception $e){
                    $err = strstr($e->getMessage(),'uplicate')?'Você já se candidatou a esta vaga.':'Erro ao enviar formulário.';
                    $err.= (APPLICATION_ENV=='development')?'<br/>'.$e->getMessage():'';
                    $this->messenger->addMessage($err,'error');
                }

                return $this->_redirect('contato');
            }
            
            $this->messenger->addMessage($this->view->translate('campo_status_preencha'),'error');
            return $this->_redirect('contato');
        }
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
    }

}