<?php

class IndexController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        // models
        $this->paginas = new Application_Model_Db_PaginaHome();
        $this->projetos = new Application_Model_Db_Portfolio();
    }

    public function indexAction()
    {
        // $pagina = Is_Array::utf8DbRow(
        //     $this->paginas->fetchRow('id = 1')
        // );
        $pagina = $this->view->pagina_home;

        $pagina->fotos = $this->paginas->getFotos($pagina->id);
        
        $this->view->pagina = $pagina;
        $this->view->titulo = $pagina->{'titulo_'.$this->view->lang};

        $this->view->projetos = $this->projetos->getLastProjects(3);
    }
    
    public function mailAction()
    {
        try{
            Trupe_Olea_Mail::send('patrick@trupe.net','Icko','Confirmação','Confirmar leitura do texto');
            //Trupe_Olea_Mail::send('icko.s@hotmail.com','Icko','Confirmação','Confirmar leitura do texto');
            //Trupe_Olea_Mail::send('patrick@ickostudios.com','Icko','Confirmação','Confirmar leitura do texto');
            echo "ok";
        } catch(Exception $e){
            echo $e->getMessage();
        }
        exit();
    }
    
    public function newsletterAction()
    {
        if($this->_request->isPost()){
            $post = $this->_request->getPost();
            $post = $post['newsletter'];
            $validator = new Zend_Validate_EmailAddress();
            
            if(trim($post['nome']) == '' ||
               trim($post['nome']) == 'nome' ||
               trim($post['telefone']) == '' ||
               trim($post['telefone']) == 'telefone' ||
               trim($post['telefone']) == '(__)____-____' ||
               strlen(trim(Is_Str::removeCaracteres($post['telefone']))) != 10 ||
               !$validator->isValid($post['email'])){
                return array("error"=>1,"message"=>"* Preencha todos os campos");
            } else {
                try {
                    $promocoes = new Application_Model_Db_Promocoes();
                    $promocoes_row = $promocoes->fetchRow($promocoes->select()->order('id desc'));
                    
                    $data = array_map('utf8_decode',$post);
                    $data['data_cad'] = date('Y-m-d H:i:s');
                    $data['telefone'] = Is_Str::removeCaracteres($data['telefone']);
                    $data['promocao_id'] = $promocoes_row ? $promocoes_row->id : null;
                    //unset($data['submit']);
                    $table = new Application_Model_Db_Mailling();
                    $table->insert($data);
                    
                    $html = '<h1 style="font-size:14px">Novo cadastro</h1><p style="font-size:11px">'.
                        '<b>Nome:</b> '.$post['nome'].'<br />'.
                        '<b>E-mail:</b> <a href="mailto:'.$post['email'].'">'.$post['email'].'</a><br />'.
                        '<b>Telefone:</b> <a href="mailto:'.$post['telefone'].'">'.$post['telefone'].'</a><br />'.
                        '</p>';
                    
                    $html2 = '<h1 style="font-size:14px">Obrigado!</h1><p style="font-size:11px">'.
                        // 'Obrigado por se cadastrar na promoção "'.($promocoes_row?utf8_encode($promocoes_row->titulo):'').'".<br/>'.
                        // 'Ao realizarmos o sorteio, informaremos so você foi a ganhadora!<br/><br/>'.
                        // 'Confira abaixo seus dados cadastrais, e boa sorte! <br /><br />'.
                        '<b>Nome:</b> '.$post['nome'].'<br />'.
                        '<b>E-mail:</b> <a href="mailto:'.$post['email'].'">'.$post['email'].'</a><br />'.
                        '<b>Telefone:</b> <a href="mailto:'.$post['telefone'].'">'.$post['telefone'].'</a><br />'.
                        '</p>';
                    
                    Trupe_Olea_Mail::sendWithReply(
                        $post['email'],
                        $post['nome'],
                        'Novo cadastro',
                        $html
                    );
                    
                    Trupe_Olea_Mail::send(
                        $post['email'],
                        $post['nome'],
                        'Cadastro efetuado com sucesso',
                        $html2
                    );
                    
                    return array("message"=>"Cadastrado! Obrigado pela participação.");
                    $form->reset();
                } catch(Exception $e){
                    $msg = strstr($e->getMessage(),'uplicate') ? '* Você já se cadastrou' : '* Erro ao enviar';
                    return array("error"=>1,"message"=>$msg);
                }
            }
        }
    }
    
}