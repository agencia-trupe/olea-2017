<?php

class ImgController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }
    
    /**
     * Index do controlador
     * - Gera thumb com base na url (parâmetros passados via querystring)
     *
     * @param string $img - caminho da imagem
     * @param int    $w   - largura da thumb
     * @param int    $h   - altura da thumb
     * @param bool   $noadapt - se definido, a imagem não será cropada
     *
     * @return image - imagem da thumb criada
     */
    public function indexAction()
    {
        $r   = array("\\","|",":",";"); // substitutos para DIRECTORY_SEPARATOR na URL por motivos de rewrite do Zend
        $p   = SCRIPT_RETURN_PATH.ROOT_PATH."/public/images/"; // path padrão de imagens
        
        // se o parâmetro 'img' não for passado ou o arquivo não existir, pega uma imagem padrão
        $img = $this->_hasParam('img') ? $p.str_replace($r,"/",$this->_getParam('img')) : $p."not-found.jpg";
        $img = is_file($img) && file_exists($img) ? $img : $p."not-found.jpg";
        
        $w = $this->_hasParam('w') ? $this->_getParam('w') : null; // define largura
        $h = $this->_hasParam('h') ? $this->_getParam('h') : null; // define altura
        
        $thumb_name = $this->getThumbName($img,$w,$h); // gera nome da thumb
        $thumb_name = substr_count($_SERVER['SCRIPT_NAME'],'/')== 2 ? '..'.$thumb_name : $thumb_name;
        //$this->checkCache(stat(SCRIPT_RETURN_PATH.$thumb_name));
        
        if(is_file($thumb_name) && file_exists($thumb_name)){ // thumb existe, somente exibe
            $this->checkCache(stat($thumb_name));
            
            // mostrando imagem com a class
            //$thumb = Php_Thumb_Factory::create($_SERVER['DOCUMENT_ROOT'].$thumb_name);
            //$thumb->show();
            
            // retirado o uso da classe p/ melhor performance
            header("Content-type: image/".$this->getExt($thumb_name));
            // readfile($_SERVER['DOCUMENT_ROOT'].$thumb_name);
            readfile($thumb_name);
            
            // include
            //include($thumb_name);
            
            // redirecionando p/ imagem
            //if(APPLICATION_ENV!='development') $thumb_name = str_replace('..','',$thumb_name);
            //$url = 'http://'.$_SERVER['HTTP_HOST'].(strpos($thumb_name,'.')===0?substr($thumb_name,1):$thumb_name);
            //header("Location: ".$url);
            
            // base64
            //header("Content-type: image/".$this->getExt($thumb_name));
            //echo 'data:image/jpg;base64,'.base64_encode(file_get_contents($_SERVER['DOCUMENT_ROOT'].$thumb_name));
        } else { // thumb não existe, então cria, salva e exibe
            $thumb = Php_Thumb_Factory::create($img);
            if($w && $h){
                ($this->_hasParam('noadapt')) ? $thumb->resize($w,$h) : $thumb->adaptiveResize($w,$h);
            }
            $thumb->save($thumb_name)->show();
        }
        exit();
    }
    
    /**
     * Gera nome da thumb com base no caminho _ largura x altura
     *
     * @param string $img - caminho da imagem
     * @param int    $w   - largura da thumb
     * @param int    $h   - altura da thumb
     *
     * @return string - nome/caminho da nova thumb 
     */
    public function getThumbName($img,$w=null,$h=null)
    {
        $ext      = ".".$this->getExt($img); // pega a extensão
        return str_replace(array($ext,'..'),'',$img). // retira extensão e ".." do nome
               ($w && $h ? "_".$w."x".$h : "").$ext; // concatena largura,altura,ext
    }
    
    /**
     * Pega extensão da imagem
     *
     * @param string $img - caminho da imagem
     *
     * @return string - extensão da imagem
     */
    public function getExt($img)
    {
        return end(explode(".",$img));
    }
    
    /**
     * Checa cache do arquivo
     *
     * @param array $FileInfos - stat() do arquivo
     */
    function checkCache($FileInfos=null)
    {
        if($FileInfos === null){
            return false;
        }
        // Checking if the client is validating his cache and if it is current.
        //$FileInfos = stat(SCRIPT_RETURN_PATH.$thumb_name);
        
        header('Cache-Control: public, must-revalidate, max-age=3600');
        header("Pragma: public");
        //header('Vary: Accept');
        
        $ClientHeaders= function_exists('apache_request_headers') ? apache_request_headers() : array();
        if (isset($ClientHeaders["If-Modified-Since"]) && (@strtotime($ClientHeaders["If-Modified-Since"]) == $FileInfos[9])) {
            // Client's cache IS current, so we just respond '304 Not Modified'.
            header("Last-Modified: " . gmdate("D, d M Y H:i:s", $FileInfos[9]) . " GMT", true, 304);
        } else {
            // Image not cached or cache outdated, we respond '200 OK' and output the image.
            header("Last-Modified: " . gmdate("D, d M Y H:i:s", $FileInfos[9]) . " GMT", true, 200);
            //header("Content-Type: " . $ContentType); 
            //header("Content-Length: " . $FileInfos[7]);
            //header("Content-Disposition: filename=" . basename($File));
            //readfile($File);
        }
        //Is_Var::dump($FileInfos,false);
        //Is_Var::dump($ClientHeaders);
    }
}