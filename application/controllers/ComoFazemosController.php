<?php

class ComoFazemosController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        $this->paginas = new Application_Model_Db_Paginas();
    }

    public function indexAction()
    {
        $pagina = Is_Array::utf8DbRow(
            // $this->paginas->fetchRow('alias="como-fazemos"')
            $this->paginas->fetchRow('alias="como-fazemos2"')
        );

        // $pagina->fotos = $this->paginas->getFotosFixas($pagina->id);
        
        $titulo = 'titulo_'.$this->view->lang;
        $this->view->pagina = $pagina;
        $this->view->titulo = $pagina->{$titulo};

        $this->view->etapas = $this->paginas->getEtapas($pagina->id);
    }
    
}