<?php

class BlogController extends ZendPlugin_Controller_Ajax
{
    public $blog_id = 1;

    public function init()
    {
        // if(APPLICATION_ENV!='development') $this->_redirect('/');

        // models
        // $this->noticias = new Application_Model_Db_Diario();
        // $this->comentarios = new Application_Model_Db_DiarioComments();
        // $this->usuarios = new Application_Model_Db_Usuario();
        // $this->view->table = new Application_Model_Db_Diario();
        
        $this->blogs = new Application_Model_Db_Blogs();
        $this->noticias = new Application_Model_Db_BlogsPosts();
        $this->comentarios = new Application_Model_Db_BlogsComments();
        $this->categorias = new Application_Model_Db_CategoriasBlog();
        $this->usuarios = new Application_Model_Db_Clientes();
        // $this->login = $this->view->login = Application_Model_Login::getLoggedUser();
        $this->login = $this->view->login = new Zend_Session_Namespace(SITE_NAME.'_login_cliente');

        $this->view->blog = $this->blog = Is_Array::utf8DbRow($this->blogs->fetchRow('id='.$this->blog_id));

        $this->view->titulo = 'Blog';
        $this->view->meta_description = $this->blog->titulo.': '.$this->blog->descricao;

        // $this->view->bitly = new Php_Bitly(BITLY_USER,BITLY_KEY);
        // $this->_helper->layout->setLayout('blog');

        // if(APPLICATION_ENV != 'development') $this->_redirect('/');
    }

    public function indexAction()
    {
        if((bool)$this->blog->require_login_posts && !Application_Model_Login::isLogged()){
            Application_Model_Login::setRedirectUrl($this);
            $this->_redirect('login');
        }

        // noticias da lateral (arquivos)
        $where_arq = 'select data_cad,count(*) as cnt from blogs_posts ';
        $where_arq.= 'where status_id = 1 ';
        $where_arq.= 'group by year(data_cad) desc, month(data_cad) desc';
        $noticias = $this->noticias->q($where_arq);
        $this->view->arquivo = count($noticias) ? ($noticias) : false;

        $anos = array();
        foreach ($noticias as $noticia){
            $ano = reset(explode('-',$noticia->data_cad));
            $anos[$ano] = $ano;
            // $anos[] = $ano;
        }
        // $anos = array_unique($anos);
        $this->view->arquivo_anos = $anos;
        $this->view->arquivo_ano = reset($anos);
        // _d(reset($anos));

        $where = 'blog_id = '.$this->blog_id;
        $where.= ' and status_id = 1';

        // busca
        if($this->_hasParam('busca')){
            $post = $this->_request->getParams();
            $post['busca'] = utf8_decode(addslashes(trim($post['busca'])));
            $this->view->busca = $post['busca'];
            
            // montando o where
            $where.= ' and (titulo like \'%'.implode('%',explode(' ',$post['busca'])).'%\'';
            $where.= ' or body like \'%'.implode('%',explode(' ',$post['busca'])).'%\')';
        }

        // filtro de categoria
        if($this->_hasParam('categoria')){
            $post = $this->_request->getParams();
            $cat_alias = utf8_decode(addslashes(trim($post['categoria'])));
            $cat = $this->categorias->fetchRow('alias = "'.$cat_alias.'"');
            
            // montando o where
            $where.= ' and categoria_id = "'.$cat->id.'"';
        }

        // filtro de arquivo
        if($this->_hasParam('arquivo')){
            $post = $this->_request->getParams();
            $arq = utf8_decode(addslashes(trim($post['arquivo'])));
            
            // montando o where
            $where.= ' and data_cad like "'.$arq.'%"';

            $this->view->arquivo_ano = reset(explode('-',$post['arquivo']));
            $this->view->arquivo_selected = $post['arquivo'];
        }

        if(!$this->_hasParam('noticia')){
            /* paginação */
            $records_per_page   = 5;
            $selectable_pages   = 10;
            $pagination = new Php_Zebra_Pagination();
            $limit  = $records_per_page;
            $offset = (($pagination->get_page() - 1) * $records_per_page);
            
            $rows = $this->noticias->fetchAll($where,'id desc',$limit,$offset);
            $total = $this->view->total = $this->noticias->count($where);
            
            /* seta parâmetros da paginação */
            $pagination->records($total)
                       ->records_per_page($records_per_page)
                       ->selectable_pages($selectable_pages)
                       ->padding(false);
            
            $this->view->paginacao = $pagination;
            
            $noticias = Is_Array::utf8DbResult($rows);

            if(count($noticias)){
                foreach ($noticias as &$noticia){
                    // pegando fotos de cada noticia
                    $noticia->fotos = $this->noticias->getFotos($noticia);
                    $noticia->categoria = $this->noticias->getCategoria($noticia);

                    // contando comentáros
                    /*$comentarios = $this->comentarios->q('select count(*) as cnt from blogs_comments where blog_post_id = "'.$noticia->id.'" and status = 1');
                    $noticia->comentarios_count = $comentarios[0]->cnt;*/
                }
            }

            $this->view->noticias = $noticias;

            return;
        }
        
        $this->view->noticia = $this->noticias->fetchRow($where.' and alias="'.$this->_getParam('noticia').'"');
        if(!$this->view->noticia || @$this->view->noticia->status_id == 0){
            $this->_forward('not-found','error','default',array('url'=>URL.'/'));
            return false;
        }
        
        $this->view->noticia = Is_Array::utf8DbRow($this->view->noticia);
        $this->view->noticia->fotos = $this->noticias->getFotos($this->view->noticia);
        $this->view->noticia->categoria = $this->noticias->getCategoria($this->view->noticia);

        // comentarios count
        $comentarios = $this->comentarios->q('select count(*) as cnt from blogs_comments where blog_post_id = "'.$this->view->noticia->id.'" and status = 1');
        $this->view->noticia->comentarios_count = $comentarios[0]->cnt;
        
        // comentários
        // se blog requer login p/ postas, checa se usuário está logado
        /*if((bool)$this->blog->require_login_comments && !Application_Model_Login::isLogged()){
            Application_Model_Login::setRedirectUrl($this);
            $this->view->comments_show = false;
        } else {
            $this->view->comments_show = true;
            $comments = Is_Array::utf8DbResult($this->comentarios->fetchAll('blog_post_id='.$this->view->noticia->id,'id'));

            foreach($comments as &$comment){
                $comment->usuario = (bool) $comment->user_cad ?
                                    Is_Array::utf8DbRow($this->usuarios->fetchRow('id='.$comment->user_cad)):
                                    null;
            }

            $this->view->comments = $comments;
        }*/
        
        // meta tags
        $titulo = 'titulo_'.$this->view->lang;
        $body = 'body_'.$this->view->lang;
        $this->view->titulo = $this->view->noticia->{$titulo};
        $this->view->meta_description = Is_Str::crop($this->view->noticia->{$titulo}.': '.Php_Html::toText($this->view->noticia->{$body}),300);
        $this->view->meta_keywords = Is_Str::text2keywords(
            Php_Html::toText($this->view->noticia->{$titulo}),', ',10
        );
        $this->view->meta_canonical = URL.'/'.$this->blog->alias.'/'.$this->view->noticia->alias;

        // meta og
        $this->view->meta_og_title = $this->view->noticia->{$titulo};
        $this->view->meta_og_description = Is_Str::crop(Php_Html::toText($this->view->noticia->{$body}),200);
        $this->view->meta_og_url = URL.'/'.$this->blog->alias.'/'.$this->view->noticia->alias;
        if(count($this->view->noticia->fotos)){
            $this->view->meta_og_image = URL.'/img/blogs-posts:'.$this->view->noticia->fotos[0]->path.'?w=80&h=80';
        }

        // posts relacionados
        $this->view->posts_relacionados = $this->noticias->getRelatedPosts($this->view->noticia,5);
    }
    
    public function comentarAction()
    {
        switch (true) {
            case !$this->_request->isPost():
            case !$this->_hasParam('blog_post_id'):
            // case !Application_Model_Login::checkAuth($this,$this->blog->alias.'_comentar'):
            case $this->blog->require_login_comments && !Application_Model_Login::isLogged():
                $error = 'Acesso negado'; break;
            case !$diario = $this->noticias->fetchRow('id='.addslashes($this->_getParam('blog_post_id'))):
                $error = 'Post não encontrado'; break;
            case strlen(trim($this->_getParam('comentario'))) == 0:
                $error = 'Escreva um comentário'; break;
            default:
                $error = null;
        }

        if($error !== null){
            if($this->isAjax()) return array('error'=>1,'msg'=>'* '.$error);
            $this->_forward('denied','error','default',array('url'=>URL.'/'.$this->blog->alias,'msg'=>$error));
            return;
        }

        $data = Is_Array::deUtf8All($this->_request->getPost());
        $data['blog_post_id'] = $this->_getParam('blog_post_id');
        $data['status']   = $this->blog->moderacao_comments==1 ? 0 : 1;
        $data['user_cad'] = (bool)@$this->login->user ? $this->login->user->id : null;
        $data['data_cad'] = date('Y-m-d H:i:s');

        if((bool)@$this->login->user) if(isset($data['nome'])) unset($data['nome']);
        if(!(bool)@$this->login->user) if(!(bool)$data['nome']) unset($data['nome']);

        try {
            $insert = $this->comentarios->insert($data);

            if($this->isAjax()){
                return array('msg'=>'Comentário enviado!','comment_id'=>$insert);
            } else {
                echo Js::alert('Comentário enviado!');
                // echo Js::location(URL.'/diario-de-viagem/'.$diario->alias);
            }
        } catch(Exception $e){
            if($this->isAjax()){
                return array('error'=>1,'msg'=>'* Erro ao enviar comentário','err'=>$e->getMessage());
            } else {
                echo Js::alert('Erro ao enviar comentário');
                echo Js::location(URL.'/'.$this->blog->alias.'/'.$diario->alias);
            }
        }
    }

    public function newCatRow($alias=null,$title=null,$posts=array())
    {
        $c = new stdClass();
        $c->alias = $alias;
        $c->title = $title;
        $c->posts = $posts;
        return $c;
    }
}