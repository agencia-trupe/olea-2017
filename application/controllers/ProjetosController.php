<?php

class ProjetosController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        $this->portfolio = new Application_Model_Db_Portfolio();
        $this->fotos = new Application_Model_Db_Fotos();
    }

    public function indexAction()
    {
        /*$rows = $this->portfolio->fetchAll('status_id=1',array('ano desc','data_edit desc'));
        
        if(count($rows)){
            $rows = Is_Array::utf8DbResult($rows);
            $rows = $this->portfolio->getFotos($rows);
        }
        
        $this->view->rows = $rows;*/
        $this->view->rows = $this->portfolio->getLastProjects();
    }
    
}