<?php

class ErrorController extends Zend_Controller_Action
{

    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');
        if (!$errors) {
            $this->view->message = 'Você encontrou nossa página de erro!';
            return;
        }
        $this->view->titulo = "ERRO";//.$errors->type;
        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $this->view->message = 'PÁGINA NÃO ENCONTRADA';
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $this->view->message = 'ERRO INTERNO DA APLICAÇÃO';
                break;
        }
        
        // Log exception, if logger available
        if ($log = $this->getLog()) {
            $log->crit($this->view->message, $errors->exception);
        }
        
        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }
        
        $this->view->request   = $errors->request;
        
        $router = new Zend_Controller_Router_Rewrite();
		$request =  new Zend_Controller_Request_Http();
		$router->route($request); // pegar todos os parametros
        $module = $request->getModuleName() == 'default' ? '' : $request->getModuleName();
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/".$module."";
    }

    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (!$bootstrap->hasResource('Log')) {
            return false;
        }
        $log = $bootstrap->getResource('Log');
        return $log;
    }
    
    public function deniedAction()
    {
        $errors = $this->_getParam('error_handler');
        $this->_helper->viewRenderer('error');
        
		$this->getResponse()->setHttpResponseCode(403);
		$this->view->titulo  = "ERRO";//.$errors->type;
        $this->view->message = $this->_hasParam('msg') ? $this->_getParam('msg') : 'ACESSO NEGADO';
		$this->view->url = $this->_url = $this->_hasParam('url') ? $this->_getParam('url') : null;
        
		if (!$errors) {
            return;
        }
        
        $this->view->request = $errors->request;
        
        $router = new Zend_Controller_Router_Rewrite();
		$request =  new Zend_Controller_Request_Http();
		$router->route($request); // pegar todos os parametros
    }
    
    public function notFoundAction()
    {
        $errors = $this->_getParam('error_handler');
        $this->_helper->viewRenderer('error');
        
		$this->getResponse()->setHttpResponseCode(404);
		$this->view->titulo  = "ERRO";//.$errors->type;
        $this->view->message = $this->_hasParam('msg') ? $this->_getParam('msg') : 'PÁGINA NÃO ENCONTRADA';
		$this->view->url = $this->_url = $this->_hasParam('url') ? $this->_getParam('url') : null;
        
		if (!$errors) {
            return;
        }
        
        $this->view->request = $errors->request;
        
        $router = new Zend_Controller_Router_Rewrite();
		$request =  new Zend_Controller_Request_Http();
		$router->route($request); // pegar todos os parametros
    }
}

