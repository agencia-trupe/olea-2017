<?php

class IdeacaoController extends ZendPlugin_Controller_Ajax
{
    private $pagina_id = 3;

    public function init()
    {
        $this->paginas = new Application_Model_Db_PaginaHome();
        $this->ideacao_passos = new Application_Model_Db_IdeacaoPassos();
        $this->messenger = new Helper_Messenger();

        $this->view->section = $this->section = "ideacao";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/".$this->section."/";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".FILE_PATH."/".$this->section;
    }

    public function indexAction()
    {
        $pagina = Is_Array::utf8DbRow(
            $this->paginas->fetchRow('id = '.$this->pagina_id)
        );

        $pagina->fotos = $this->paginas->getFotos($pagina->id);
        
        $titulo = 'titulo_'.$this->view->lang;
        $this->view->pagina = $pagina;
        $this->view->titulo = $pagina->{$titulo};
        
        $this->view->ideacao_passos = $this->paginas->getIdeacaoPassos($pagina->id,false);
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
    }
    
}