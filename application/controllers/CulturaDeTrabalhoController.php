<?php

class CulturaDeTrabalhoController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        $this->paginas = new Application_Model_Db_Paginas();
    }

    public function indexAction()
    {
        $pagina = Is_Array::utf8DbRow(
            $this->paginas->fetchRow('alias="cultura-de-trabalho"')
        );

        $pagina->fotos = $this->paginas->getFotosFixas($pagina->id);
        
        $titulo = 'titulo_'.$this->view->lang;
        $this->view->pagina = $pagina;
        $this->view->titulo = $pagina->{$titulo};
    }
    
}