<?php

class ProjetoController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        $this->portfolio = new Application_Model_Db_Portfolio();
        $this->fotos = new Application_Model_Db_Fotos();
    }

    public function indexAction()
    {
        $alias = $this->view->alias = addslashes($this->_getParam('alias'));

        $row = Is_Array::utf8DbRow(
        	$this->portfolio->fetchRow('alias = "'.$alias.'"')
    	);

    	if(!$row) return $this->_redirect('projetos');
        
        $row = $this->portfolio->getFotos($row);
        $this->view->row = $row;

        $this->view->outros_projetos = $this->portfolio->q(
        	'select titulo_pt, titulo_en, alias from portfolio '.
        	'where status_id = 1 '.
        	'order by ano desc, data_edit desc'
        );
    }
    
}