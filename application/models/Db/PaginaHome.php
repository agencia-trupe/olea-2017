<?php
/**
 * Modelo da página home
 */
class Application_Model_Db_PaginaHome extends ZendPlugin_Db_Table 
{
    protected $_name = "pagina_home";
    
    protected $_dependentTables = array('Application_Model_Db_PaginaHomeFotos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_PaginaHomeFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_PaginaHomeFotos',
            'refColumns'    => 'pagina_id'
        )
    );
    
    /**
     * Retorna as fotos do produto
     *
     * @param int $id - id do produto
     *
     * @return array - rowset com fotos do produto
     */
    public function getFotos1($id)
    {
        if(!$produto = $this->fetchRow('id="'.$id.'"')) return false;
        $fotos = array();
        
        if($produto_fotos = $produto->findDependentRowset('Application_Model_Db_PaginaHomeFotos')){
            foreach($produto_fotos as $produto_foto){
                $fotos[] = Is_Array::utf8DbRow($produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
            }
        }
        
        return $fotos;
    }

    /**
     * Retorna as fotos do produto
     *
     * @param int $id - id do produto
     *
     * @return array - rowset com fotos do produto
     */
    public function getFotos($id,$tipo=null)
    {
        return $this->q(
            'select phf.pagina_id, phf.tipo, f.* from pagina_home_fotos phf '.
            'left join fotos f on f.id = phf.foto_id '.
            'where phf.pagina_id="'.$id.'"'.($tipo?' and tipo="'.$tipo.'" ':' ').
            'order by id '
        );
    }

    public function getCaracteristicas($id=null,$withFotos=true)
    {
        $table = new Application_Model_Db_Caracteristicas();
        $where = 'pagina_id in ('.$id.')';
        
        $rows = $table->fetchAll($where,'ordem');
        $rows = Is_Array::utf8DbResult($rows);

        if($withFotos){
            $ids = array();

            foreach ($rows as $row) $ids[] = $row->id;

            $_fotos = $table->q(
                'select ef.caracteristica_id, f.* from caracteristicas_fotos ef '.
                'left join fotos f on f.id = ef.foto_id '.
                'where ef.caracteristica_id in('.(count($ids) ? implode(',',$ids) : '0').') '
            );

            $fotos = array();
            foreach($_fotos as $f){
                if(!isset($fotos[$f->caracteristica_id])) $fotos[$f->caracteristica_id] = array();
                $fotos[$f->caracteristica_id][] = $f;
            }

            foreach ($rows as &$row){
                $row->fotos = isset($fotos[$row->id]) ? $fotos[$row->id] : array();
            }
        }

        return $rows;
    }

    public function getIdeacaoPassos($id=null,$withFotos=true)
    {
        $table = new Application_Model_Db_IdeacaoPassos();
        $where = 'pagina_id in ('.$id.')';
        
        $rows = $table->fetchAll($where,'ordem');
        $rows = Is_Array::utf8DbResult($rows);

        if($withFotos){
            $ids = array();

            foreach ($rows as $row) $ids[] = $row->id;

            $_fotos = $table->q(
                'select ef.ideacao_passo_id, f.* from ideacao_passos_fotos ef '.
                'left join fotos f on f.id = ef.foto_id '.
                'where ef.ideacao_passo_id in('.(count($ids) ? implode(',',$ids) : '0').') '
            );

            $fotos = array();
            foreach($_fotos as $f){
                if(!isset($fotos[$f->ideacao_passo_id])) $fotos[$f->ideacao_passo_id] = array();
                $fotos[$f->ideacao_passo_id][] = $f;
            }

            foreach ($rows as &$row){
                $row->fotos = isset($fotos[$row->id]) ? $fotos[$row->id] : array();
            }
        }

        return $rows;
    }
}