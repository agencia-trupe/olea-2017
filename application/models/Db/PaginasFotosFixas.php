<?php
/**
 * Modelo da tabela de usuarios
 */
class Application_Model_Db_PaginasFotosFixas extends Zend_Db_Table 
{
    protected $_name = "paginas_fotos_fixas";
    
    protected $_dependentTables = array('Aluno_Model_Pessoa');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Pessoa' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Pessoa',
            'refColumns'    => 'usuario_id'
        )
    );
}