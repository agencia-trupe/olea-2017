<?php

class Application_Model_Db_Caracteristicas extends ZendPlugin_Db_Table 
{
    protected $_name = "caracteristicas";
    
    /**
     * Retorna registro por alias
     */
    public function getByAlias($alias)
    {
        return $this->fetchRow('alias = "'.$alias.'"');
    }

    /**
     * Retorna as fotos do noticia
     *
     * @param int $id - id do noticia
     *
     * @return array - rowset com fotos do noticia
     */
    public function getFotos($id,$cor=null)
    {
        if(!$noticia = $this->fetchRow('id="'.$id.'"')) return null;
        $fotos = array();
        
        if($noticia_fotos = $noticia->findDependentRowset('Application_Model_Db_EtapasFotos')){
            foreach($noticia_fotos as $noticia_foto){
                $f = Is_Array::utf8DbRow($noticia_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
                // $f->cor_id = $noticia_foto->cor_id ? $noticia_foto->cor_id : null;
                // $f->cor    = $noticia_foto->cor_id ? Is_Array::utf8DbRow($noticia_foto->findDependentRowset('Application_Model_Db_Cores')->current()) : null;
                
                if($cor!==null) {
                    if ($cor==$f->cor_id) $fotos[] = $f;
                } else {
                    $fotos[] = $f;
                }
            }
        }
        
        return $fotos;
    }

    /**
     * Retorna as tags do noticia
     *
     * @param int  $id        - id do noticia
     * @param int  $limit     - limite da lista
     * @param bool $rand      - ordenação randômica?
     *
     * @return array - rowset com sugestoes do noticia
     */
    public function getTags($id,$limit=null,$rand=false)
    {
        $_sugestoes = new Application_Model_Db_EtapasTags();
        $_tags = new Application_Model_Db_Tags();
        $sugestoes = array();

        if(!(bool)$prods = $_sugestoes->fetchAll('etapa_id="'.$id.'"')){
            return false;
        }

        foreach($prods as $ps){
            $s = Is_Array::utf8DbRow($_tags->fetchRow('id='.$ps->tag_id));
            $sugestoes[] = $s;
        }
        
        return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;
    }

    /**
     * Retorna as tags do noticia pr alias
     *
     * @param int  $alias     - alias da tag
     * @param int  $limit     - limite da lista
     * @param bool $rand      - ordenação randômica?
     *
     * @return array - rowset com sugestoes do noticia
     */
    public function getTagsByAlias($alias,$limit=null,$rand=false)
    {
        $_sugestoes = new Application_Model_Db_EtapasTags();
        $_tags = new Application_Model_Db_Tags();
        $sugestoes = array();
        $alias = is_array($alias) ? implode('","',$alias) : $alias;

        if(!(bool)$tag = $_tags->fetchAll('alias in ("'.$alias.'")')){
            return false;
        }

        $tag_ids = array();
        foreach($tag as $t) $tag_ids[] = $t->id;

        if(!(bool)$prods = $_sugestoes->fetchAll('tag_id in ("'.(implode('","',$tag_ids)).'")')){
            return false;
        }

        foreach($prods as $ps){
            array_push($sugestoes,$ps->etapa_id);
        }
        
        return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;
    }
    
}