<?php

class Application_Model_Db_Impressao3dArquivos extends ZendPlugin_Db_Table
{
    protected $_name = "impressao3d_arquivos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Arquivos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Arquivos' => array(
            'columns' => 'arquivo_id',
            'refTableClass' => 'Application_Model_Db_Arquivos',
            'refColumns'    => 'id'
        )
    );
}