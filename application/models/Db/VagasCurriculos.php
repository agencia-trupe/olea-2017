<?php

class Application_Model_Db_VagasCurriculos extends ZendPlugin_Db_Table
{
    protected $_name = "vagas_curriculos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Arquivos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Arquivos' => array(
            'columns' => 'arquivo_id',
            'refTableClass' => 'Application_Model_Db_Arquivos',
            'refColumns'    => 'id'
        )
    );
}