<?php

class Application_Model_Db_EtapasArquivos extends Zend_Db_Table {
    protected $_name = "etapas_arquivos";
    
    protected $_dependentTables = array('Application_Model_Db_Etapas');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Etapas' => array(
            'columns' => 'etapa_id',
            'refTableClass' => 'Application_Model_Db_Etapas',
            'refColumns'    => 'id'
        )
    );
}