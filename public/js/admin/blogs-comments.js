var MAX_FOTOS = 0, MAX_SIZE = 5242880, allow_photos;

$(document).ready(function(){
    $('textarea.wysiwyg').wysiwyg({
        css:{fontFamily:'Arial,sans-serif',fontSize:'12px',color:'#3C3C3C',margin:0,padding:0},
        visible:"bold,italic,underline,separator00,insertUnorderedList,insertOrderedList,separator01,"+
                "createLink"+(allow_photos?",insertImage":"")+",separator02,undo,redo,separator03,removeFormat"
    });
    $('div.wysiwyg,div.wysiwyg iframe').css({'width':'495px'});
    $('div.wysiwyg').css({'margin-left':'110px'});
    $('div.wysiwyg div').remove();
    
    $("#frm-"+DIR).submit(function(e){
        if($.trim($("#titulo").val()).length == 0){
            $("#titulo").focus();
            Msg.add("Preencha todos os campos corretamente.","erro").show(5000);
            return false;
        }
        
        return true;
    });
    
    $("#submit").click(function(e){
        $("#frm-"+DIR).submit(e);
    });

    $("#search-by").change(function(){
        if($(this).val() == "blog_post_id"){
            var $combo = $('<select></select>');
            $combo.attr({
                'name'  : 'search-txt',
                'id'    : 'search-txt',
                'style' : 'width: 180px;'
            }).addClass('txt');
            
            if(blog_posts){
                for(i in blog_posts){
                    $combo.append('<option value="'+i+'">'+blog_posts[i]+'</option>');
                };
            }
            
            $("#search-txt").replaceWith($combo);
        } else if($(this).val() == "user_cad"){
            var $combo = $('<select></select>');
            $combo.attr({
                'name'  : 'search-txt',
                'id'    : 'search-txt',
                'style' : 'width: 180px;'
            }).addClass('txt');
            
            if(usuarios){
                for(i in usuarios){
                    $combo.append('<option value="'+i+'">'+usuarios[i]+'</option>');
                };
            }
            
            $("#search-txt").replaceWith($combo);
        } else if($("#search-txt").attr("type").indexOf('select') != -1){
            var $combo = $('<input/>');
            $combo.attr('name','search-txt').attr('id','search-txt').addClass('txt');
            $("#search-txt").replaceWith($combo);
        }
    });
    
    head.js(JS_PATH+'/Messenger.min.js');
    $('.chk_status').change(function(){
        // console.log(Number($(this).is(':checked'))+','+$(this).data('id'));
        var $this = $(this),
            data = {
                status : Number($this.is(':checked')),
                id : $this.data('id')
            };

        $this.attr('disabled',true);
        $.post(GLOBAL_URL+'/save-status.json',data,function(json){
            // console.log(json);
            Messenger.add(json.msg,(json.error==1?'error':'message')).show(1000);
            if(json.error) alert(json.error);

            $this.attr('disabled',false);
        },'json');
    });
});