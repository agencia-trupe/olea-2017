var PHOTO_DESCRIPTION = false;

$(document).ready(function () {
	//console.log(PHOTO_DESCRIPTION);
    /* Upload fotos galeria */
    $('#file_upload2').fileUploadUI({
        uploadTable: $('#file_upload_list2'),
        downloadTable: $('.list_fotos2'),
        buildUploadRow: function (files, index) {
            //alert(files[index].size);
			//return $('<tr><td class="filename">' + files[index].name + '<\/td>' +
            return $('<tr>'+
					//'<td class="filename file_upload_preview"><\/td>' +
					'<td class="file_upload_progress"><div></div></td>' +
					'<td class="filesize">'+Files.formatBytes(files[index].size)+'</td>' +
                    '<td class="file_upload_cancel">' +
                    '<button class="ui-state-default ui-corner-all" title="Cancel">' +
                    '<span class="ui-icon ui-icon-cancel">Cancel<\/span>' +
                    '<\/button>' +
					'<\/td>' +
					//'<td class="file_delete">' +
					//'<button title="Delete" class="ui-state-default ui-corner-all file-delete">' +
					//'<span class="ui-icon ui-icon-trash"><\/span>' +
					//'<\/button>' +
					//'<\/td>' +
					'<\/tr>');
        },
        buildDownloadRow: function (file) {
            //return $('<tr><td>' + file.name + '<\/td><\/tr>');
            if(file.error){
				return $('<tr><td><span>' + file.error + '<\/span><\/td><\/tr>');
			} else {
				return $('<li><a href=\'' + URL+'/img/'+DIR+":"+file.name + '\' rel=\'gallery_group2\'>'+
						 '<img src=\'' + URL+'/img/'+DIR+":"+file.name + '?w=90&h=90\'><\/a>'+ // 3 fotos
						 //'<img src=\'' + URL+'/img/'+DIR+":"+file.name + '?w=65&h=65\'><\/a>'+ // 4 fotos
						 '<input type="text" '+
							'name="foto_descricao[]" '+
							'class="txt foto_descricao prevalue" '+
							'id="foto_descricao_'+file.id+'" '+
							'value="" '+
							'data-prevalue="Descrição..." '+
							'data-foto-id="'+file.id+'" '+
							'data-pagina-id="'+ID+'" \/>'+
						 '<button title="Delete" class="ui-state-default ui-corner-all file-delete">' +
						 '<span class="ui-icon ui-icon-trash"><\/span>' +
						 '<\/button>' +
						 ((allow_photos) ? '<button title="Inserir esta imagem no texto" class="ui-state-default ui-corner-all file-insert">'+
							'<span class="ui-icon ui-icon-arrowthick-1-w"></span>'+
							'</button>' : '') +
						 '<input type="hidden" name="foto_id[]" value="'+file.id+'" class="foto_id" />' +
						 '<span class="foto_status"><\/span>'+
						 '<\/li>');
			}
        },
		beforeSend: function(event, files, index, xhr, handler, callBack){
			$('#file_upload_list2 tr.error').remove();
			
			var patt_img  = /.*(gif|jpeg|png|jpg)$/i,///(jpeg|jpg|png|gif|bmp)/gi,
				file_name = files[index].name,
				max_size  = (MAX_SIZE)  ? MAX_SIZE  : 2097152,
				max_files = (MAX_FOTOS) ? MAX_FOTOS : 0,
				file_count= $(".list_fotos2 li").size() + $('#file_upload_list2 tr').size(),
				max_count = (max_files>0) ? file_count <= max_files : true,
				msg       = "Erro ao enviar imagem";
			//console.log(file_name+","+Files.getExt(file_name)+","+patt_img.test(Files.getExt(file_name)));
			//console.log(files[index].size < max_size);
			//console.log(max_count);
			//return false;
			if(patt_img.test(Files.getExt(file_name)) && files[index].size < max_size && max_count){
				callBack();
				return true;
			}
			
			/*switch(false){
				case patt_img.test(Files.getExt(file_name)):
					msg = "Imagem inválida"; break;
				case files[index].size < max_size:
					msg = "Tamanho máx. permitido: "+Files.formatBytes(max_size); break;
				case max_count:
					msg = "Você pode enviar no máximo "+max_files+" fotos"; break;
					//msg = "Erro ao enviar imagem"; break;
				default:
					callBack();
					return true;
			}*/ 
			msg = !patt_img.test(Files.getExt(file_name)) ? "Imagem inválida" : msg;
			msg = files[index].size >= max_size ? "Tamanho máx. permitido: "+Files.formatBytes(max_size) : msg;
			msg = !max_count ? "Você pode enviar no máximo "+max_files+" fotos" : msg;
			
			handler.cancelUpload(event, files, index, xhr, handler);
			handler.addNode($('#file_upload_list2'),
				$('<tr class="error"><td colspan="3">'+msg+' ('+file_name+')</td></tr>')
			);
			//callBack();
		},
		onComplete: function (event, files, index, xhr, handler) {
			// var json = handler.response;
			Gallery.show();
			$('.foto_descricao.prevalue').prevalue();
		}
    });
    
    $("#files2 tr").mouseover(function(){
        $(this).addClass("hover");
    }).mouseout(function(){
        $(this).removeClass("hover");
    });
	
	$("#file_upload_list2 tr.error").live('click',function(){
		$this = $(this);
		
		$this.fadeOut("fast",function(){
			$this.remove();
		})
	});
});
