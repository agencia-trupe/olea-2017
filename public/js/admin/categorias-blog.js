/*global $ */
var MsgRow = Messenger;
//MsgRow.container = $(".row-tools");
MsgRow.elm       = ".row-tools .row-status";

$(document).ready(function(){
    ajust_order();
    
    $('#categorias .row-list .row-order').live('click',function(){
        if(!$(this).hasClass('disabled')){
            var row = $(this).parent();
            if($(this).hasClass('order-down')){
                row.insertAfter(row.next());
            }else{
                if(row.prev().attr("class") != "title"){
                    row.insertBefore(row.prev());
                }
            }
            ajust_order();
        }
    });
    
    $('#categorias .row-list').sortable({
        revert:true,
        start:function(e,ui){
            $('#categorias .row-list li').addClass('disabled');
            ui.item.removeClass('disabled').addClass('grabber');
        },
        stop:function(e,ui){
            $('#categorias .row-list li').removeClass('disabled').removeClass('grabber');
            ajust_order();
        }
    });
    $('#categorias .row-list li.title').draggable({revert:'invalid'});
    //$('#categorias .row-list li').disableSelection();
    
    $('.row-status_id').live('click',function(){
        var row = $(this).parent();
        row.find('input[name="status_id[]"]').val(($(this).is(':checked')?"1":"0"));
    });
	
	$(".row-add").click(function(){
		Rows.add();
	});
	$(".row-action-del").live('click',function(){
		Rows.del($(this).parent());
	});
    $(".frm-categorias .bt").click(function(){
        Rows.save();
    });
});

function ajust_order(){
    $('#categorias .row-list .row-ordem').each(function(i,v){
        $(this).val(i+1);
    });
    
    $('#categorias .row-list .row-order').removeClass('disabled');
    $('#categorias .row-list .order-up:first,#categorias .row-list .order-down:last').addClass('disabled');
}

Rows = {
	add: function(){
		var newRow = $("#tmplRow").tmpl().appendTo(".row-list");
		//newRow.find('input').val("");
		newRow.find('input.row-descricao_pt').focus();
        ajust_order();
	},
	
	del: function(row){
		if(confirm("Deseja remover o registro?")){
			var id        = row.find(".row-id");
			
			if($.trim(id.val()).length > 0){
				var url = URL+"/admin/"+DIR+"/del.json?id="+id.val();
				$.getJSON(url,function(data){
					if(data.error){
						alert(data.error);
					} else {
						Rows.remove(row);
					}
				});
			} else {
				Rows.remove(row);
			}
		}
	},
	
	remove: function(row){
		row.remove();
		MsgRow.add("Registro removido.").show(null,3000);
        ajust_order();
	},
	
	save: function(){
		$('.frm-categorias').submit();
	}
}