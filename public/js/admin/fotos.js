/*global $ */
$(document).ready(function () {
    $('#file_upload').fileUploadUI({
        uploadTable: $('#file_upload_list'),
        downloadTable: $('.list_fotos'),
        buildUploadRow: function (files, index) {
            //alert(files[index].size);
			//return $('<tr><td class="filename">' + files[index].name + '<\/td>' +
            return $('<tr>'+
					//'<td class="filename file_upload_preview"><\/td>' +
					'<td class="file_upload_progress"><div></div></td>' +
					'<td class="filesize">'+Files.formatBytes(files[index].size)+'</td>' +
                    '<td class="file_upload_cancel">' +
                    '<button class="ui-state-default ui-corner-all" title="Cancel">' +
                    '<span class="ui-icon ui-icon-cancel">Cancel<\/span>' +
                    '<\/button>' +
					'<\/td>' +
					//'<td class="file_delete">' +
					//'<button title="Delete" class="ui-state-default ui-corner-all file-delete">' +
					//'<span class="ui-icon ui-icon-trash"><\/span>' +
					//'<\/button>' +
					//'<\/td>' +
					'<\/tr>');
        },
        buildDownloadRow: function (file) {
            //return $('<tr><td>' + file.name + '<\/td><\/tr>');
            var $list_fotos = $('.list_fotos'),
            	has_param = $list_fotos.hasClass('has-param'),
            	has_lang = $list_fotos.hasClass('has-lang');

            if(file.error){
				return $('<tr><td><span>' + file.error + '<\/span><\/td><\/tr>');
			} else {
				if(has_lang){
					var l, langs_select = '<select ';
					langs_select+= 'name="foto_lang_'+file.id+'" ';
					langs_select+= 'id="foto_lang_'+file.id+'" ';
					langs_select+= 'class="txt foto_lang '+($('.foto_lang').hasClass('no-margin')?'no-margin':'')+'" ';
					langs_select+= 'data-id="'+file.id+'" ';
					langs_select+= 'data-name="lang">';

					langs_select+= '<option value="__none__">Idioma...</option>';
					for(l in LANGS) langs_select+= '<option value="'+LANGS[l]+'">'+LANGS_DESC[l]+'</option>';

					langs_select+= '</select>';
				}

				return $('<li><a href=\'' + URL+'/img/'+DIR+":"+file.name + '\' rel=\'gallery_group\'>'+
						 '<img src=\'' + URL+'/img/'+DIR+":"+file.name + '?w=90&h=90\'><\/a>'+ // 3 fotos
						 //'<img src=\'' + URL+'/img/'+DIR+":"+file.name + '?w=65&h=65\'><\/a>'+ // 4 fotos
						 
						 ((DIR=='home' || DIR=='impressao3d') ? 
						 	'<input type="text" name="foto_link[]" class="txt foto_link" id="foto_link_'+file.id+'" value="" data-id="'+file.id+'" data-name="param" />' :
						 	'')+

						 ((has_lang) ? langs_select : '')+

						 '<button title="Delete" class="ui-state-default ui-corner-all file-delete">' +
						 '<span class="ui-icon ui-icon-trash"><\/span>' +
						 '<\/button>' +
						 ((allow_photos) ? '<button title="Inserir esta imagem no texto" class="ui-state-default ui-corner-all file-insert">'+
							'<span class="ui-icon ui-icon-arrowthick-1-w"></span>'+
							'</button>' : '') +
						 
						 ((DIR=='home' || DIR=='impressao3d' || has_param) ? 
						 	'<input type="hidden" name="foto_id[]" value="'+file.id+'" class="foto_id" data-name="id" /><span class="foto_status"></span>' :
					        '<input type="hidden" name="foto_id[]" value="'+file.id+'" class="foto_id" />') +
						 
						 // ((has_param) ? '<span class="foto_status"></span>' : '')+
						 
						 '<\/li>');
			}
        },
		beforeSend: function(event, files, index, xhr, handler, callBack){
			$('#file_upload_list tr.error').remove();
			
			var patt_img  = /.*(gif|jpeg|png|jpg)$/i,///(jpeg|jpg|png|gif|bmp)/gi,
				file_name = files[index].name,
				max_size  = (MAX_SIZE)  ? MAX_SIZE  : 2097152,
				max_files = (MAX_FOTOS) ? MAX_FOTOS : 0,
				file_count= $(".list_fotos li").size() + $('#file_upload_list tr').size(),
				max_count = (max_files>0) ? file_count <= max_files : true,
				msg       = "Erro ao enviar imagem";
			//console.log(file_name+","+Files.getExt(file_name)+","+patt_img.test(Files.getExt(file_name)));
			//console.log(files[index].size < max_size);
			//console.log(max_count);
			//return false;
			if(patt_img.test(Files.getExt(file_name)) && files[index].size < max_size && max_count){
				callBack();
				return true;
			}
			
			/*switch(false){
				case patt_img.test(Files.getExt(file_name)):
					msg = "Imagem inválida"; break;
				case files[index].size < max_size:
					msg = "Tamanho máx. permitido: "+Files.formatBytes(max_size); break;
				case max_count:
					msg = "Você pode enviar no máximo "+max_files+" fotos"; break;
					//msg = "Erro ao enviar imagem"; break;
				default:
					callBack();
					return true;
			}*/ 
			msg = !patt_img.test(Files.getExt(file_name)) ? "Imagem inválida" : msg;
			msg = files[index].size >= max_size ? "Tamanho máx. permitido: "+Files.formatBytes(max_size) : msg;
			msg = !max_count ? "Você pode enviar no máximo "+max_files+" fotos" : msg;
			
			handler.cancelUpload(event, files, index, xhr, handler);
			handler.addNode($('#file_upload_list'),
				$('<tr class="error"><td colspan="3">'+msg+' ('+file_name+')</td></tr>')
			);
			//callBack();
		},
		onComplete: function (event, files, index, xhr, handler) {
			// var json = handler.response;
			Gallery.show();

			if(DIR=='home' || DIR=='impressao3d') _prevalueLinks();
		}
    });
    
    $("button.file-delete").live('click',function(){
        var row = $(this).parents("li");
		// Files.del(row,false,$(this).hasClass('file'));
		Fotos.del(row,false,$(this).hasClass('file'));
    });
	$("button.file-insert").live('click',function(){
        var row = $(this).parents("li");
		Files.insert(row);
    });
	$("button.save-all").live('click',function(){
        var row = $(this).parents("li");
		Files.saveAll(row);
    });
    
    $("#files tr").mouseover(function(){
        $(this).addClass("hover");
    }).mouseout(function(){
        $(this).removeClass("hover");
    });
	
	$("#file_upload_list tr.error").live('click',function(){
		$this = $(this);
		
		$this.fadeOut("fast",function(){
			$this.remove();
		})
	});

	// links
	_prevalueLinks();

    $('input.foto_link').live('keyup',function(e){
        // if(e.which==13) Files.saveAll($(this).parents('li'));
        if(e.which==13) Fotos.saveAll($(this).parents('li'));
    });

    $('select.foto_lang').live('change',function(e){
        Fotos.saveAll($(this).parents('li'));
    });
});

function _prevalueLinks(){
	$('input.foto_link').each(function(i,v){
        var $t = $(this);
        if($.trim($t.val())=='') $t.prevalue('URL...');
    });
}

var Files = {
    formatBytes: function(size){
        var units = new Array(' B', ' KB', ' MB', ' GB', ' TB');
        for (var i = 0; size >= 1024 && i < 4; i++) size /= 1024;
        return Math.round(size)+units[i];
    },

    saveAll: function(row){
    	var data         = {},
    		inputs       = row.find('input'),
    		selects      = row.find('select'),
    		url          = GLOBAL_URL+'save-all.json',
            status       = row.find('.foto_status');
    		invalid_vals = [
    						'',
    						'...',
    						// '__none__',
			    			'título...',
			    			'descrição...',
			    			'selecione...',
			    			'url...'
			    		];

    	inputs.each(function(){
    		var $t = $(this);
    		// data['id'] = $t.data('id');
    		if($t.data('name') && $.inArray($.trim($t.val().toLowerCase()),invalid_vals)==-1)
    			data[$t.data('name')] = $.trim($t.val());
    	});

    	selects.each(function(){
    		var $t = $(this);
    		data['id'] = $t.data('id');
    		if($t.data('name') && $.inArray($t.val().toLowerCase(),invalid_vals)==-1)
    			data[$t.data('name')] = $t.val();
    	});

        status.html('Aguarde...').show();
        
        $.post(url,data,function(json){
            if(json.error){
                alert(json.error);
                status.html('').hide();
                return false;
            }
            
            status.html(json.msg||'Concluído').delay(3000).fadeOut('slow');
        },'json');
    },
    
    del: function(row,dir,file){
        dir = dir || false;

        switch(DIR){
        	case 'blogs-posts':
        		var url = GLOBAL_URL+"/fotos-del.json?file="+row.find("input.foto_id").val(); break;
        	default:
        		var url = URL+"/admin/"+DIR+"/fotos-del.json?file="+row.find("input.foto_id").val();
        }
        
        var confirm_text = file ?
        				   "Deseja deletar o arquivo selecionado?" : 
        				   "Deseja deletar a foto selecionada?";

        if(confirm(confirm_text)){
            $.getJSON(url,function(data){
                if(data.error){
                    alert(data.error);
                } else {
                    row.fadeOut("slow",function(){ $(this).remove(); });
                }
            });
        }
    },
	
	insert: function(row){
		var path = row.find('a').attr('href');
		$('textarea.wysiwyg').wysiwyg('insertImage', path);
		_contentImgSize();
	},
	
	getExt: function(name){
		return name.split(".").reverse()[0];
	}
}

var Fotos = Files;