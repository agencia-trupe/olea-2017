var Msg = Messenger;
Msg.container = $("div.main .content");

$(document).ready(function(){
    //head.js(JS_PATH+'/less.js');
    
    $("a[href^='#']").click(function(e){
        e.preventDefault();
    });
	
	$('input[type=checkbox],input[type=radio]').css('border','none');
    
    var menu_time = "fast";
    $(".admin-menu .navigation ul").addClass("submenu");
    $(".admin-menu .navigation ul ul").removeClass("submenu").addClass("submenusub");
    
    $(".admin-menu a.has-sub").append(' &darr;');
    $(".admin-menu a.has-sub").mouseenter(function(){
        clearTimeout($(this).data('timeoutId'));
        $(this).parent().find("ul.submenu").slideDown(menu_time);
    }).mouseleave(function(){
        var someelement = this;
        var timeoutId = setTimeout(function(){ $(someelement).parent().find("ul.submenu").slideUp(menu_time);}, 200);
        $(someelement).data('timeoutId', timeoutId);
    });
    $("ul.submenu").mouseenter(function(){
        clearTimeout($(this).parent().find("a.has-sub").data('timeoutId'));
    }).mouseleave(function(){
        $(this).slideUp(menu_time);
    });

    $(".admin-menu a.has-sub-sub").append(' &rarr;');
    $(".admin-menu a.has-sub-sub").mouseenter(function(){
        clearTimeout($(this).data('timeoutId'));
        $(this).parent().find("ul.submenusub").fadeIn(menu_time);
    }).mouseleave(function(){
        var someelement = this;
        var timeoutId = setTimeout(function(){ $(someelement).parent().find("ul.submenusub").fadeOut(menu_time);}, 200);
        $(someelement).data('timeoutId', timeoutId);
    });
    $("ul.submenusub").mouseenter(function(){
        clearTimeout($(this).parent().find("a.has-sub-sub").data('timeoutId'));
    }).mouseleave(function(){
        $(this).slideUp(menu_time);
    });
    
    $("input.txt,select.txt,textarea.txt").live('focus',function(){
        $(this).addClass('hover');
    }).live('blur',function(){
        $(this).removeClass('hover');
    });
    
    $(".row-list li").live("mouseover",function(){
		$(this).find("a.row-action").show();
	}).live("mouseout",function(){
		$(this).find("a.row-action").hide();
	});
	
	if(!$("#flash-messages ul li").size()){
        var $flash = $("#flash-messages");
        $flash.hide();
    }

    _onlyNumbers('.mask-int');

    // tabs-lang
    var $tabsLang = $('.tabs-lang');
    if($tabsLang.length > 0){
        head.js(JS_PATH+'/is.simple.tabs.lang.js',function(){
            $('.tabs-lang').isSimpleTabsLang();
        });
    }
});

/* Alias */
(function($){  
    $.fn.getAlias = function(from) {
        var t = $(this),
            f = $(from);
        
        f.bind('keyup',function(){
            t.val('/'+normalize(f.val())+'/');
        });
        
        var normalize = function(str){
            return str.toLowerCase().replace(/[^a-z0-9]+/g,'-');
        }
    }
})(jQuery);

/* currency */
function currency(v){ return 'R$ '+(v+'').replace('.',','); }

/* in_main */
function in_main(n){ return $('div.main').hasClass(n); }

/* nl2br */
function nl2br(v){ return v.replace(/\n/g,'<br />'); }

/* urlencode.min */
function urlencode(str){var histogram={},tmp_arr=[];var ret=(str+"").toString();var replacer=function(search,replace,str){var tmp_arr=[];tmp_arr=str.split(search);return tmp_arr.join(replace)};histogram["'"]="%27";histogram["("]="%28";histogram[")"]="%29";histogram["*"]="%2A";histogram["~"]="%7E";histogram["!"]="%21";histogram["%20"]="+";histogram["\u20AC"]="%80";histogram["\u0081"]="%81";histogram["\u201A"]="%82";histogram["\u0192"]="%83";histogram["\u201E"]="%84";histogram["\u2026"]="%85";histogram["\u2020"]="%86";histogram["\u2021"]="%87";histogram["\u02C6"]="%88";histogram["\u2030"]="%89";histogram["\u0160"]="%8A";histogram["\u2039"]="%8B";histogram["\u0152"]="%8C";histogram["\u008D"]="%8D";histogram["\u017D"]="%8E";histogram["\u008F"]="%8F";histogram["\u0090"]="%90";histogram["\u2018"]="%91";histogram["\u2019"]="%92";histogram["\u201C"]="%93";histogram["\u201D"]="%94";histogram["\u2022"]="%95";histogram["\u2013"]="%96";histogram["\u2014"]="%97";histogram["\u02DC"]="%98";histogram["\u2122"]="%99";histogram["\u0161"]="%9A";histogram["\u203A"]="%9B";histogram["\u0153"]="%9C";histogram["\u009D"]="%9D";histogram["\u017E"]="%9E";histogram["\u0178"]="%9F";ret=encodeURIComponent(ret);for(search in histogram){replace=histogram[search];ret=replacer(search,replace,ret)}return ret.replace(/(\%([a-z0-9]{2}))/g,function(full,m1,m2){return"%"+m2.toUpperCase()});return ret};

/* onlyNumbers */
function _onlyNumbers(selector){
    $(selector).keypress(function(e){
        return (e.which >= 48 && e.which <= 57) || ($.inArray(e.which,[0,13,8]) != -1);
    });
}

function _contentImgSize(){
    // console.log('img');
    $('div.wysiwyg,div.wysiwyg iframe').contents().find('img').css({'max-width':'98%'});
}