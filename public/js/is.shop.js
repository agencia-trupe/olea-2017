$.extend({
	isShop:{
		defaultOpt: {
			messageAddToCart: "Produto adicionado ao carrinho!",
			url: URL+"/meu-carrinho/",
			addUrl: URL+"/meu-carrinho/add.json",
			removeUrl: URL+"/meu-carrinho/remove.json",
			countUrl: URL+"/meu-carrinho/count-itens.json",
		},
		
		init: function(opt){
			$.extend(this.defaultOpt,opt);
			
			var o = this.defaultOpt;
			
			$(".shop-close-message-box").live("click",function(){
				$.isShop.closeMessageBox();
			});
			
			return this;
		},
		
		messageBox: function(html){
			var y = (document.documentElement.scrollTop) ? document.documentElement.scrollTop : document.body.scrollTop;
			
			var bgDiv = $('<div class="shop-overlay"></div>')
						.css({height: $(document).height(),display:"none"})
						.bind("click",function(){
							$.isShop.closeMessageBox();
						})
						.prependTo('body'),
				msgDiv = $('<div class="shop-message-box"><div class="shop-message-content"></div></div>')
						.css({top: y,left:"50%",display:"none"})
						.prependTo('body');
			
			$(".shop-message-content").append(html);
			$(".shop-message-box").css({
				marginTop:  ($(window).height() / 6)+"px",
				marginLeft: ("-"+$(".shop-message-box").width() / 2)+"px"
			});
			
			if($.browser.msie){
				$(".shop-overlay").show();$(".shop-message-box").fadeIn("fast");
			} else {
				$(".shop-overlay,.shop-message-box").fadeIn("fast");
			}
			return this;
		},

		messageBoxList: function(html){
			(typeof(Messenger) != 'undefined') ?
				// console.log($this.defaultOpt.messageAddToList) : console.log('erro');
				Messenger.add(html || this.defaultOpt.messageAddToList).show(3000) :
				alert(html || this.defaultOpt.messageAddToList);

			console.log('ok');
		},
		
		closeMessageBox: function(callBack){
			$(".shop-overlay,.shop-message-box").fadeOut("fast",function(){
				$(".shop-overlay,.shop-message-box").remove();
			});
			return this;
		},
		
		addToCart: function(item){
			var $this = this;
			$.getJSON($this.defaultOpt.url+'add.json',item,function(json){
				if(json.error){
					alert(json.error);
					return false;
				}
				
				$this.messageBox($this.defaultOpt.messageAddToCart);
				$this.countItems(1,true);
				return $this;
			});
		},
		
		removeFromCart: function(item,callBack){
			var $this = this;
			$.getJSON($this.defaultOpt.url+'remove.json',item,function(json){				
				if(typeof(callBack) === 'function'){
					callBack();
				}
			});
			return $this;
		},
		
		calculeTotal: function(){
			var total = 0, $this = this, amount, price, $row;
			
			$('.cart-item').each(function(){
				$row = $(this);
				
				if($row.find('input.item-valor').size()){
					amount = parseInt($row.find('input.item-amount').val()),
					price  = parseFloat($row.find('input.item-valor').val());
					
					if(total == 0){
						$this.resetCount();
					}
					
					total+= parseFloat(price*amount);
					$this.countItems(amount);
					// console.log(price+"*"+amount+"="+total);
				}
			});
			
			//$('.cart-item-delivery').each(function(){
			//	total+= parseFloat($(this).find('.item-delivery-rate').val());
			//});
			
			// console.log(total);
			return total;
		},
		
		resetCount: function(){
			var $counter = $('#cart-counter');
			$counter.html('');
			return this;
		},
		
		countItems: function(count,noUpdate){
			var $counter = $('#cart-counter'),
				val = parseInt($counter.text());
			//console.log(val);
			val = val > 0 ? val : 0;
			
			var add = val === 0 && noUpdate;
			//if(val === 0 && noUpdate){ return this; }
			
			val+= count;
			$counter.html(add ? ' ('+val+')' : val);
			return this;
		},
		
		countItemsList: function(count,noUpdate){
			var $counter = $('#carrinho-lista-n'),
				val = parseInt($counter.text());
			//console.log(val);
			val = val > 0 ? val : 0;
			
			var add = val === 0 && noUpdate;
			//if(val === 0 && noUpdate){ return this; }
			
			val+= count;
			$counter.html(add ? val : val);
			return this;
		},

		checkRates: function(total,rates){
			var $divrate, inc = false, dec = false, rate, checked, optional;
			total = parseFloat(total);
			
			for(i in rates){
				inc = false; dec = false; rate = rates[i];
				$divrate = $('#rate_'+rate.id);
				checked = $divrate.find('.choose-rate').attr('checked');
				optional = $divrate.hasClass('rate-optional');
				
				rate.isencao = parseFloat(rate.isencao);
				rate.valor = parseFloat(rate.valor);
				
				if(rate.isencao != 0 && optional){
					if(rate.isencao > total){
						//console.log(rate.isencao+' > '+total);
						if(checked) total+= rate.valor;
						inc = true;
					} else if(rate.isencao < total){
						//console.log(rate.isencao+' < '+total);
						dec = true;
					}
				} else if(rate.isencao != 0 && !optional){
					if(rate.isencao > total){
						//console.log(rate.isencao+' > '+total);
						total+= rate.valor;
						inc = true;
					} else if(rate.isencao < total){
						//console.log(rate.isencao+' < '+total);
						dec = true;
					}
				} else if(rate.isencao == 0 && optional){
					//console.log(rate.isencao+' > '+total);
					if(checked) total+= rate.valor;
					//inc = true;
				} else if(rate.isencao == 0 && !optional){
					//console.log(rate.isencao+' > '+total);
					total+= rate.valor;
					inc = true;
				} else if(rate.isencao > 0 && !optional){
					if(rate.isencao > total){
						//console.log(rate.isencao+' > '+total);
						total+= rate.valor;
						inc = true;
					} else if(rate.isencao < total){
						//console.log(rate.isencao+' < '+total);
						dec = true;
					}
				}

				if(rate.isencao > 0){
					if($divrate.hasClass('rate-disabled')){
						if(inc) $divrate.removeClass('rate-disabled');
					} else {
						if(dec) $divrate.addClass('rate-disabled');
					}
				}
			}

			if(!$('#choose-rate_frete').attr('checked')){
				var campoFrete = $('.item-valor-total-frete');

				if(campoFrete.length) total-= campoFrete.val();
			}
			
			return total;
		},
		
		checkRates1: function(total,rates){
			var $divrate, inc = false, dec = false;
			total = parseFloat(total);
			
			for(i in rates){
				inc = false; dec = false;
				$divrate = $('#rate_'+rates[i].id);
				rates[i].isencao = parseFloat(rates[i].isencao);
				rates[i].valor = parseFloat(rates[i].valor);
				
				if(rates[i].isencao > total){
					//console.log(rates[i].isencao+' > '+total);
					total+= rates[i].valor;
					inc = true;
				} else if(rates[i].isencao < total){
					//console.log(rates[i].isencao+' < '+total);
					dec = true;
				}
				
				if($divrate.is(':hidden')){
					if(inc) $divrate.show();
				} else {
					if(dec) $divrate.hide();
				}
			}
			
			return total;
		},

		// funções para impressão 3D
		showFormImpressao3d: function(msg){
			var $this = this;
			$this.messageBox(msg || $this.defaultOpt.messageAddToCart);
		}
	}
});