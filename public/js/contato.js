// tabs de formulários
head.js(JS_PATH+'/is.simple.tabs.js',function(){
    $('div.tabs').isSimpleTabs();
});

// enviando contato
head.js(JS_PATH+'/is.simple.validate.js',function(){
    $('form.contato').submit(function(e){
        e.preventDefault();
        var $form = $(this),
            $status = $form.find('.status').removeClass('error').html('');
        $status.html(TRANSLATE.campo_status_enviando);
        
        if($form.isSimpleValidate()){
        //if(validadeForm($form)){
            var url  = URL+'/'+CONTROLLER+'/enviar.json',
                data = $form.serialize();
            
            $.post(url,data,function(json){
                if(json.error){
                    $status.addClass('error').html('* '+json.error);
                    return false;
                }
                
                $status.html(json.msg);
            },'json');
        } else {
            $status.addClass('error').html('* '+TRANSLATE.campo_status_preencha);
        }
        
        return false;
    });
});

// enviando trabalhe conosco
$.getCss(JS_PATH+'/fileinput/fileinput.css');
head.js(JS_PATH+'/fileinput/jquery-ui-1.8.9.custom.min.js',JS_PATH+'/fileinput/jquery.fileinput.min.js',function(){
    $('#arquivo').fileinput({
        inputText: TRANSLATE.campo_arquivo_input,
        buttonText: TRANSLATE.campo_arquivo_button
    });
});

function _testForm(formSelector) {
    var $form = $(formSelector || 'form.contato');

    $('input[type=text], textarea', $form).val('Teste');
    $('#email', $form).val(SITE_NAME+'@mailinator.com');
    $('#ddd', $form).val('11');
    $('#tel', $form).val('11111111').blur();

    $form.submit();
}