Gallery = {
	show: function(){
		$("a[rel=gallery_group]"+
		",a[rel=gallery_group1],a[rel=gallery_group2],a[rel=gallery_group3],a[rel=gallery_group4],a[rel=gallery_group5]"+
		",a[rel=gallery_group6],a[rel=gallery_group7],a[rel=gallery_group8],a[rel=gallery_group9],a[rel=gallery_group10]"+
		",a[rel=gallery_group11],a[rel=gallery_group12],a[rel=gallery_group13],a[rel=gallery_group14],a[rel=gallery_group15]"+
		",a[rel=gallery_group16],a[rel=gallery_group17],a[rel=gallery_group18],a[rel=gallery_group19],a[rel=gallery_group20]"+
		",a[rel=gallery_group21],a[rel=gallery_group22],a[rel=gallery_group23],a[rel=gallery_group24]").fancybox({
			'transitionIn'		: 'none',
			'transitionOut'		: 'none',
			'titlePosition' 	: 'over',
			'titleFormat'       : function(title, currentArray, currentIndex, currentOpts) {
				var html = '<span id="fancybox-title-over">'+
						   '<font style="color:#aaa">Imagem '+(currentIndex + 1)+' de '+currentArray.length+'</font> '+
						   (title.length > 0 ? "- "+title : "")+
						   '</span>';
				
				return html;
			}
		});
	}
}

$(document).ready(function(){
    Gallery.show();
});