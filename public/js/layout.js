var fbLoaded;

(function($){
    if(APPLICATION_ENV == 'development') head.js(JS_PATH+'/less.js',function(){
        // less.watch();
        var timeLess = 3.5;
        if(1) intLess = window.setInterval(function(){
            localStorage.clear();
            less.refresh();
        },timeLess*1000);
    }); // load less in dev

    // máscaras padrões
    $("input.mask-cpf").mask("999.999.999-99");
    $("input.mask-cep").mask("99999-999");
    $("input.mask-data,input.mask-date").mask("99/99/9999");
    $("input.mask-tel").mask("(99)9999-9999");
    $("input.mask-currency").maskCurrency();
    _onlyNumbers('.mask-int');

    $(".prevalue").each(function(){ $(this).prevalue(); }); // prevalue em campos com a class .prevalue

    $("a[href^='#']").live("click",function(e){ // remove click de links vazios (que começam por #)
        e.preventDefault();
    });
    $("a.js-back").live("click",function(e){ // adiciona ação de voltar a links com a classe js-back
        history.back();
    });

    // ativa menus
    var menus_ativar = {
        '/projetos/' : 5,
        '/projeto/' : 5,
        '/blog/' : 7
    };

    for(i in menus_ativar){
        if(location.href.indexOf(i)!=-1){
            var $li = $('#top nav ul li').eq(menus_ativar[i]);
            $li.addClass('active').find('a').addClass('active');
        }
    }

    if($(".pagination").size()){ // se tiver paginação
        $(".pagination .navigation.left").html("&laquo;"); // arruma texto da navegação
        $(".pagination .navigation.right").html("&raquo;");
        if($(".pagination a").size() <= 1){
            $(".pagination a").hide(); // se tiver somente 1 ou nenhuma página esconde a div
        }
    }

    // flash-messages
    if($("#flash-messages").size()){
        var $flash = $("#flash-messages");
        var y = (document.documentElement.scrollTop) ? document.documentElement.scrollTop : document.body.scrollTop;
        $flash.css({top:y+10}).delay(1000).show().click(function(){
            $(this).fadeOut("slow");
        });
    }

    // scroll to
    var $scrollTos = $('a.scroll-to');
    if($scrollTos.length){
        head.js(JS_PATH+'/jquery.scrollTo-min.js',function(){
            $scrollTos.live('click',function(e){
                var $this = $(this);
                $.scrollTo($($this.attr('href')),1000,{axis:'y'});
            });
        });
    }

    // img preload
    var $preloadImgs = $('[data-preload-img]');
    if($preloadImgs.length){
        $preloadImgs.each(function(){
            _preloadImg($(this).data('preload-img'));
        });
    }

    if(SCRIPT) head.js(JS_PATH+'/'+SCRIPT+'.js'); // carrega scripts de páginas

    // controller specific
    switch(CONTROLLER){
        case 'index':
        _banners();
        break; 

        case 'impressao-3d':
        $('#top .impressao3d').addClass('active');
        _banners();
        break;

        case 'projeto':
        $projeto_imgs = $('.projeto img');
        
        if($projeto_imgs.length){
            $projeto_imgs.each(function(){
                var $this = $(this), a = '';

                a+= '<a ';
                a+= 'href="'+$this.attr('src').replace(URL+'/img/projetos|',IMG_PATH+'/projetos/')+'" ';
                // a+= 'title="'+$this.data('descricao')+'" ';
                a+= 'rel="projeto_group" ';
                a+= 'class="fb-image" ';
                a+= '></a>';

                $this.wrap(a);
            });

            _loadFancybox(function(){
                $('a[rel=projeto_group]').fancybox({
                    'titlePosition':'inside'
                });
            });
        }
        break;

        case 'blog':
        $('#arquivo').change(function(){
            $('#arquivos ul li a').hide();
            $('.arquivo-'+$(this).val()).show();
        });

        if(ACTION!='index' && ACTION!='categoria' && ACTION!='arquivo'){
            // twitter
            !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
            // g+
            window.___gcfg = {lang: 'pt-BR'};
            (function() {
                var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                po.src = 'https://apis.google.com/js/plusone.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
            })();
        }
        break;
    }
})(jQuery);

// --------------------------------- GENERAL FUNCTIONS --------------------------------- >

// Color Hex <-> RGB
function hexToR(h){return parseInt((cutHex(h)).substring(0,2),16)}
function hexToG(h){return parseInt((cutHex(h)).substring(2,4),16)}
function hexToB(h){return parseInt((cutHex(h)).substring(4,6),16)}
function cutHex(h){return (h.charAt(0)=="#") ? h.substring(1,7):h}
function hexToRGB(h){return {'R':hexToR(h),'G':hexToG(h),'B':hexToB(h)}}
function RGBToHex(r,g,b){var dec = r+256*g+65536*b;return dec.toString(16);}

// Math
function getRand(n){n=n||0;return Math.floor(Math.random()*(n+1));}

// ValidateForm
function validadeForm(f){var $f=$(f),err=0;$f.find("input").each(function(i,v){if($(this).data("validate")&&$.trim($(this).val())==""){alert($(this).data("errmsg"));$(this).focus();err++;return false}});if(err==0){$f.submit()}};

// onlyNumbers
function _onlyNumbers(selector){
    $(selector).keypress(function(e){
        return (e.which >= 48 && e.which <= 57) || ($.inArray(e.which,[0,13,8]) != -1);
    });
}

function _preloadImg(src){
    var img = new Image();
    img.src = src;
}

function _log(log){ return typeof(window.console)=='undefined' ? alert(log) : console.log(log); }
function _d(log){ return _log(log); }

function _banners(){
    head.js(JS_PATH+'/cycle.min.js',function(){
        $('#banners').cycle({fx:'fade',speed:3000});
    });

    var $video = $('#videos a');

    if($video.attr('href')!='#'){
        _loadFancybox(function(){
            $video.fancybox({
                type: 'iframe'
            });
        });
    }
}

function _loadFancybox(callback){
    if(fbLoaded){
        if(typeof(callback)=='function') (callback)();
    } else {
        $.getCss(CSS_PATH+'/fancybox/jquery.fancybox-1.3.4.css');
        head.js(JS_PATH+'/jquery.fancybox-1.3.4.pack.js',function(){
            fbLoaded = true;
            if(typeof(callback)=='function') (callback)();
        });
    }
}